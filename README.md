# Squad The Spire
### This is really more like a couple different mods at the same time

## Player grouping
Providing the ability for there to be "multiple" targets for enemies.
Whether that resembles allowing the player to gain members through the use of
shop purchases, abilities, or multiplayer games the idea is _roughly_ the same regardless.
This effects a number of different aspects of the game. 

- Rendering multiple party members on the user's side of the screen
- Do different monster's attacks handle there being multiple targets?
- Can a user interact with their squad mates
- probably other things I'm not thinking about

This needs to be provided, probably, as a separate API that can be utilized by other mods

## Multiplayer
Should there be steam support? Non steam support? Local co-op? Split screen? etc., etc., etc.

Initially there will probably only be steam support, but I think it would be nice to provide pleasant methods to
handle this while off platform or just on a local network and not connected to the steam API.

The multiplayer mod should also, probably, provide a pleasant API for other mod developers to utilize if they want
their mod to be handled by the multiplayer features implicitly.