package com.squedgy.slay.the.spire.encapsulation.user;

import com.megacrit.cardcrawl.blights.*;
import com.megacrit.cardcrawl.relics.*;

import java.util.function.*;

public class Players {
    private static Supplier<PlayerInformation> p = () -> null;

    public static void setPlayerInformation(Supplier<PlayerInformation> provider) {
        p = provider;
    }

    public static boolean isDead() {
        PlayerInformation player = p.get();
        if(player == null) return true;

        return player.isDead();
    }

    public static boolean hasBlight(String blight) {
        PlayerInformation player = p.get();
        if(player == null) return false;

        return  player.hasBlight(blight);
    }

    public static AbstractBlight getBlight(String blight) {
        PlayerInformation player = p.get();
        if(player == null) return null;

        return player.getBlight(blight);
    }

    public static boolean hasRelic(String relic) {
        PlayerInformation player = p.get();
        if(player == null) return false;

        return player.hasRelic(relic);
    }

    public static AbstractRelic getRelic(String relic) {
        PlayerInformation player = p.get();
        if(player == null) return null;

        return player.getRelic(relic);
    }
}
