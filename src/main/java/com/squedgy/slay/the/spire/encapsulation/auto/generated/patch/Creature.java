package com.squedgy.slay.the.spire.encapsulation.auto.generated.patch;

import java.util.IdentityHashMap;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.squedgy.slay.the.spire.encapsulation.Encapsulation;
import com.squedgy.slay.the.spire.encapsulation.auto.generated.*;
import com.evacipated.cardcrawl.modthespire.lib.*;
import javassist.*;
import javassist.expr.*;

public class Creature {

    @SpirePatches2({ @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "<ctor>", paramtypes = { "java.lang.String", "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }) })
    public static class SetDefaultPlayerWhileConstructingPlayer {

        private static final IdentityHashMap<AbstractPlayer, AbstractPlayer> instanceToPreviousPlayer = new IdentityHashMap<>();

        @SpirePrefixPatch
        public static void beforeConstruct(AbstractPlayer __instance) {
            // Set what the default player was upon starting construction
            instanceToPreviousPlayer.put(__instance, Encapsulation.getDefaultPlayer());
            // Set default player to new instance
            Encapsulation.setDefaultPlayer(__instance);
        }

        @SpirePostfixPatch
        public static void afterConstruct(AbstractPlayer __instance) {
            // Remove and retrieve previous default player for instance
            AbstractPlayer previousDefault = instanceToPreviousPlayer.remove(__instance);
            // Reset the default to expected
            Encapsulation.setDefaultPlayer(previousDefault);
        }
    }
}
