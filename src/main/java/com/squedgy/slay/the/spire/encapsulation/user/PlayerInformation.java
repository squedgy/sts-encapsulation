package com.squedgy.slay.the.spire.encapsulation.user;

import com.megacrit.cardcrawl.blights.*;
import com.megacrit.cardcrawl.relics.*;

public interface PlayerInformation {
    boolean hasBlight(String blight);
    boolean hasRelic(String relic);

    AbstractBlight getBlight(String blight);
    AbstractRelic getRelic(String relic);

    boolean isDead();
}
