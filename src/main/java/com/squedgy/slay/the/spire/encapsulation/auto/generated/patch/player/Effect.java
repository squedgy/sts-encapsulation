package com.squedgy.slay.the.spire.encapsulation.auto.generated.patch.player;

import com.squedgy.slay.the.spire.encapsulation.Encapsulation;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.vfx.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.squedgy.slay.the.spire.encapsulation.auto.generated.*;
import java.util.function.*;
import javassist.expr.*;
import com.evacipated.cardcrawl.modthespire.lib.*;
import javassist.*;
import javassist.Modifier;

@SpirePatch2(clz = com.megacrit.cardcrawl.vfx.AbstractGameEffect.class, method = com.evacipated.cardcrawl.modthespire.lib.SpirePatch.CLASS)
public class Effect {

    public static SpireField<AbstractPlayer> player = new SpireField<AbstractPlayer>(() -> null);

    public static AbstractPlayer getPlayer(AbstractGameEffect arg) {
        if (arg == null)
            return Encapsulation.getDefaultPlayer();
        AbstractPlayer tmpPlay = Effect.player.get(arg);
        if (tmpPlay == null)
            return Encapsulation.getDefaultPlayer();
        return tmpPlay;
    }

    public static void setPlayer(AbstractGameEffect arg, AbstractPlayer player) {
        if (arg == null)
            return;
        if (player == null)
            player = Encapsulation.getDefaultPlayer();
        Effect.player.set(arg, player);
    }

    @SpirePatches2({ @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.AbstractGameEffect", method = "<ctor>", paramtypes = {}) })
    public static class PatchConstructingBaseType {

        @SpirePrefixPatch
        public static void setPlayerOnConstruct(AbstractGameEffect __instance) {
            Effect.setPlayer(__instance, Encapsulation.getDefaultPlayer());
        }
    }

    @SpirePatches2({ @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.CardTrailEffect", method = "init", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.FastCardObtainEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.ObtainPotionEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.PlayerTurnEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.RelicAboveCreatureEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.SpotlightPlayerEffect", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireLiftEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireSleepEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireSmithEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireTokeEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ExhaustCardEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.BattleStartEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.DaggerSprayEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.HemokinesisEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.OfferingEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.stance.WrathStanceChangeParticle", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }) })
    public static class EffectPlayerReferences {

        @SpireInstrumentPatch()
        public static ExprEditor handleStaticPlayerReferences() {
            return com.squedgy.slay.the.spire.encapsulation.util.PatchUtilities.patchStaticPlayerReferences(Effect.class.getName().replace('$', '.') + ".getPlayer((" + AbstractGameEffect.class.getName().replace('$', '.') + ")this)");
        }
    }
}
