package com.squedgy.slay.the.spire.encapsulation.user;

import com.megacrit.cardcrawl.characters.*;
import com.squedgy.slay.the.spire.encapsulation.*;

@FunctionalInterface
public interface PlayerModification {
    /**
     * Allows modification of a player object being set. This is not guaranteed to be the original object passed to one of the
     * setters related to this modification's target.
     * <br><br>
     * EX: {@link Encapsulation#setPlayer(AbstractPlayer)} will call modifications listening through
     * {@link Encapsulation#addPlayerModifier(PlayerModification)}
     *
     * @param currentPlayer the current player queued to be set
     * @param removeMe      A function to mark this modification for removal from future sets
     *
     * @return The modified player
     */
    AbstractPlayer modify(AbstractPlayer currentPlayer, Runnable removeMe);
}
