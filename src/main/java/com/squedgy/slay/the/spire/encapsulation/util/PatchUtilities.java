package com.squedgy.slay.the.spire.encapsulation.util;

import com.megacrit.cardcrawl.characters.*;
import com.megacrit.cardcrawl.dungeons.*;
import com.squedgy.slay.the.spire.encapsulation.*;
import javassist.*;
import javassist.expr.*;

/**
 * DO NOT modify the utilities in this class unless you also modify their references in the generator project look for "CallPatchUtilities"
 */
public abstract class PatchUtilities {
    private PatchUtilities() throws IllegalAccessException {throw new IllegalAccessException("PatchUtilities should not be instantiated");}

    public static String getInlineDefaultOf(Class<?> targetType, String nullable, String defaultExpr) {
        return (
              "(" +
                  "(" + targetType.getName() + ") " +
                  "(" +
                      nullable + " instanceof " + targetType.getName() + " ? " +
                         "(" + targetType.getName() + ")" + nullable + " : " +
                          defaultExpr +
                  ")" +
              ");"
        );
    }

    public static ExprEditor patchStaticPlayerReferences(String playerFrom) {
        return new ExprEditor() {
            @Override public void edit(FieldAccess f) throws CannotCompileException {
                try {
                    CtField field = f.getField();
                    CtClass declaringType = field.getDeclaringClass();
                    boolean playerField = field.getName().equals("player");
                    boolean abstractPlayerField = field.getType().getName().equals(AbstractPlayer.class.getName());
                    boolean declaredOnAbstractDungeon = declaringType.getName().equals(AbstractDungeon.class.getName());

                    if(playerField && abstractPlayerField && declaredOnAbstractDungeon && f.isReader()) {
                        if(f.isReader()) {
                            f.replace("$_ = " + getInlineDefaultOf(AbstractPlayer.class, playerFrom, Encapsulation.class.getName() + ".getDefaultPlayer()"));
                        } else if(f.isWriter()) {
                            f.replace(Encapsulation.class.getName() + ".setPlayer($1);");
                        }
                    }
                } catch(NotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }
}
