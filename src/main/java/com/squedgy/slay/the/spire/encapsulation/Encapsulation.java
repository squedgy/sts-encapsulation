package com.squedgy.slay.the.spire.encapsulation;

import com.badlogic.gdx.*;
import com.badlogic.gdx.files.*;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.google.gson.*;
import com.megacrit.cardcrawl.characters.*;
import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
import com.megacrit.cardcrawl.dungeons.*;
import com.squedgy.slay.the.spire.encapsulation.auto.generated.patch.PlayerFields;
import com.squedgy.slay.the.spire.encapsulation.user.*;
import com.squedgy.slay.the.spire.encapsulation.util.*;

import java.io.*;
import java.nio.charset.*;
import java.util.*;

import static org.slf4j.LoggerFactory.*;

@SpireInitializer
public class Encapsulation {
    private static final org.slf4j.Logger LOG = getLogger(Encapsulation.class);
    public static final String MOD_ID = "encapulation";
    public static final String RESOURCE_BASE = "/" + MOD_ID + "Resources/";

    private static String modID;

    private static AbstractPlayer player;
    private static Optional<AbstractPlayer> defaultPlayer = Optional.empty();
    private static final LinkedList<PlayerModification> playersListeners = new LinkedList<>();

    /**
     * @return the player that is currently the target for being rendering and handling most UI updates in the base game
     */
    public static AbstractPlayer getPlayer() {
//        String name = null, clazz = null;
//        if(player != null) {
//            name = player.name;
//            clazz = player.chosenClass.name();
//        }
//        LOG.info("Getting player: {} - {}\n", name, clazz);

        return player;
    }

    public static void setPlayer(AbstractPlayer renderedPlayer) {
        Encapsulation.player = renderedPlayer;

        String name = null;
        PlayerClass clazz = null;
        if(Encapsulation.player != null) {
            Iterator<PlayerModification> mods = playersListeners.iterator();
            while(mods.hasNext()) {
                LOG.info("Modifying {} - {}\n", PlayerFields.getName(Encapsulation.player),PlayerFields.getChosenClass(Encapsulation.player));
                Encapsulation.player = mods.next().modify(Encapsulation.player, mods::remove);
            }

            name = PlayerFields.getName(Encapsulation.player);
            clazz = PlayerFields.getChosenClass(Encapsulation.player);
        }
        LOG.info("Setting player: {} - {}\n", name, clazz);

        AbstractDungeon.player = Encapsulation.player;
    }

    public static void performWithPlayer(AbstractPlayer player, Runnable runnable) {
        Optional<AbstractPlayer> curDef = defaultPlayer;
        defaultPlayer = Optional.ofNullable(player);

        runnable.run();

        defaultPlayer = curDef;
    }

    public static void setDefaultPlayer(AbstractPlayer player) {
        if(player == Encapsulation.player) defaultPlayer = Optional.empty();
        else defaultPlayer = Optional.ofNullable(player);
    }

    public static AbstractPlayer getDefaultPlayer() {
//        String defName = player == null ? null : player.name;
//        LOG.info("Getting default player {} - def {}\n", defName, defaultPlayer.map(p -> p.name));
        return defaultPlayer.orElse(player);
    }

    /**
     * Returns the given modifier in-case you might want to remove it later
     * @param modifier your modification to run when attempting to set the current player
     * @return your modification in-case you wanted to call
     * {@link #removePlayerModifier(PlayerModification) removeRenderedPlayerModifier}.
     * You could also run the second argument of {@link PlayerModification#modify(AbstractPlayer, Runnable) modify}
     * instead of calling <strong>removeRenderedPlayerModifier</strong>
     */
    public static PlayerModification addPlayerModifier(PlayerModification modifier) {
        playersListeners.add(modifier);
        return modifier;
    }

    public static void removePlayerModifier(PlayerModification modifier) {
        playersListeners.remove(modifier);
    }

    private Encapsulation() {
        LOG.info("Subscribe to BaseMod hooks\n");

//        BaseMod.subscribe(this);
        setModID(MOD_ID);

        LOG.info("Done subscribing\n");
    }

    public static void initialize() {
        LOG.info("========================= Initializing Encapsulation. Hi. =========================\n");
        Encapsulation encapsulation = new Encapsulation();
        LOG.info("========================= /Encapsulation Initialized./ =========================\n");
    }

    // this adds "ModName:" before the ID of any card/relic/power etc.
    // in order to avoid conflicts if any other mod uses the same ID.
    public static String makeID(String idText) {
        return getModID() + ":" + idText;
    }

    // ====== NO EDIT AREA ======
    // DON'T TOUCH THIS STUFF. IT IS HERE FOR STANDARDIZATION BETWEEN MODS AND TO ENSURE GOOD CODE PRACTICES.
    // IF YOU MODIFY THIS I WILL HUNT YOU DOWN AND DOWNVOTE YOUR MOD ON WORKSHOP

    private static void setModID(String ID) { // DON'T EDIT
        Gson coolG = new Gson(); // EY DON'T EDIT THIS
        //   String IDjson = Gdx.files.internal("IDCheckStringsDONT-EDIT-AT-ALL.json").readString(String.valueOf(StandardCharsets.UTF_8)); // i hate u Gdx.files
        InputStream in = Encapsulation.class.getResourceAsStream("/IDCheckStringsDONT-EDIT-AT-ALL.json"); // DON'T EDIT THIS ETHER
        IDCheckDontTouchPls EXCEPTION_STRINGS =
              coolG.fromJson(new InputStreamReader(in, StandardCharsets.UTF_8), IDCheckDontTouchPls.class); // OR THIS, DON'T EDIT IT
        LOG.info("You are attempting to set your mod ID as: {}\n", ID); // NO WHY
        if(ID.equals(EXCEPTION_STRINGS.DEFAULTID)) { // DO *NOT* CHANGE THIS ESPECIALLY, TO EDIT YOUR MOD ID, SCROLL UP JUST A LITTLE, IT'S JUST ABOVE
            throw new RuntimeException(EXCEPTION_STRINGS.EXCEPTION); // THIS ALSO DON'T EDIT
        } else if(ID.equals(EXCEPTION_STRINGS.DEVID)) { // NO
            modID = EXCEPTION_STRINGS.DEFAULTID; // DON'T
        } else { // NO EDIT AREA
            modID = ID; // DON'T WRITE OR CHANGE THINGS HERE NOT EVEN A LITTLE
        } // NO
        LOG.info("Success! ID is {}\n", modID); // WHY WOULD U WANT IT NOT TO LOG?? DON'T EDIT THIS.
    } // NO

    public static String getModID() { // NO
        return modID; // DOUBLE NO
    } // NU-UH

    private static void pathCheck() { // ALSO NO
        Gson coolG = new Gson(); // NOPE DON'T EDIT THIS
        //   String IDjson = Gdx.files.internal("IDCheckStringsDONT-EDIT-AT-ALL.json").readString(String.valueOf(StandardCharsets.UTF_8)); // i still hate u btw Gdx.files
        InputStream in = Encapsulation.class.getResourceAsStream("/IDCheckStringsDONT-EDIT-AT-ALL.json"); // DON'T EDIT THISSSSS
        IDCheckDontTouchPls EXCEPTION_STRINGS =
              coolG.fromJson(new InputStreamReader(in, StandardCharsets.UTF_8), IDCheckDontTouchPls.class); // NAH, NO EDIT
        //        String packageName = Encapsulation.class.getPackage().getName(); // STILL NO EDIT ZONE
        FileHandle resourcePathExists = Gdx.files.internal(getModID() + "Resources"); // PLEASE DON'T EDIT THINGS HERE, THANKS
        if(!modID.equals(EXCEPTION_STRINGS.DEVID)) { // LEAVE THIS EDIT-LESS
            // Removing the following 3 lines since proper package name are unsupported and this wouldn't accept com.squedgy.Encapsulation anyway
            // if (!packageName.equals(getModID())) { // NOT HERE ETHER
            //     throw new RuntimeException(EXCEPTION_STRINGS.PACKAGE_EXCEPTION + getModID()); // THIS IS A NO-NO
            // } // WHY WOULD U EDIT THIS
            if(!resourcePathExists.exists()) { // DON'T CHANGE THIS
                throw new RuntimeException(EXCEPTION_STRINGS.RESOURCE_FOLDER_EXCEPTION + getModID() + "Resources"); // NOT THIS
            }// NO
        }// NO
    }// NO

    // ====== YOU CAN EDIT AGAIN ======
}
