package com.squedgy.slay.the.spire.encapsulation.auto.generated.patch;

import java.lang.reflect.Field;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.relics.AbstractRelic;
import com.megacrit.cardcrawl.blights.AbstractBlight;
import com.megacrit.cardcrawl.helpers.PowerTip;
import java.util.Objects;
import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
import com.megacrit.cardcrawl.ui.panels.energyorb.EnergyOrbInterface;
import basemod.animations.AbstractAnimation;
import com.squedgy.slay.the.spire.encapsulation.auto.generated.*;
import com.evacipated.cardcrawl.modthespire.lib.*;
import javassist.expr.*;
import basemod.abstracts.*;
import javassist.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.function.Consumer;
import com.megacrit.cardcrawl.cards.CardGroup;
import java.util.ArrayList;
import com.megacrit.cardcrawl.core.EnergyManager;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.stances.AbstractStance;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.Color;
import com.megacrit.cardcrawl.vfx.TintEffect;
import com.megacrit.cardcrawl.core.AbstractCreature.CreatureAnimation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.screens.CharSelectInfo;
import com.megacrit.cardcrawl.potions.AbstractPotion;
import com.megacrit.cardcrawl.orbs.AbstractOrb;
import com.megacrit.cardcrawl.trials.AbstractTrial;
import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
import com.megacrit.cardcrawl.random.Random;
import java.util.UUID;
import com.megacrit.cardcrawl.monsters.MonsterGroup;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import basemod.abstracts.events.PhasedEvent;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import basemod.BaseMod;
import java.util.List;
import com.megacrit.cardcrawl.screens.stats.RunData;
import com.megacrit.cardcrawl.actions.unique.DiscoveryAction;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.cutscenes.Cutscene;
import com.megacrit.cardcrawl.screens.VictoryScreen;
import com.megacrit.cardcrawl.screens.charSelect.CharacterOption;
import com.megacrit.cardcrawl.screens.GameOverScreen;
import com.megacrit.cardcrawl.screens.mainMenu.ColorTabBar;
import com.megacrit.cardcrawl.screens.runHistory.RunHistoryScreen;
import java.util.function.Predicate;
import java.util.Map;
import com.evacipated.cardcrawl.mod.stslib.cards.targeting.TargetingHandler;
import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
import com.megacrit.cardcrawl.vfx.campfire.CampfireSmithEffect;
import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
import com.megacrit.cardcrawl.rewards.RewardItem;
import com.megacrit.cardcrawl.ui.buttons.SingingBowlButton;
import com.megacrit.cardcrawl.actions.unique.RetainCardsAction;

public class PlayerFields {

    public static String toPascalCase(String prefix, CtField field) {
        String name = field.getName();
        if (name.contains("_")) {
            String[] parts = name.split("_");
            StringBuilder builder = new StringBuilder();
            for (String s : parts) {
                builder.append(Character.toUpperCase(s.charAt(0))).append(s.substring(1));
            }
            name = builder.toString();
        } else {
            name = Objects.toString(Character.toUpperCase(name.charAt(0))) + name.substring(1);
        }
        return prefix + name;
    }

    public static CreatureHandler<?> getDefaultCreatureHandler(AbstractCreature player) {
        return new CreatureHandler() {

            @Override
            public AbstractCreature getCreature() {
                return player;
            }
        };
    }

    public static PlayerHandler<?> getDefaultPlayerHandler(AbstractPlayer player) {
        return new PlayerHandler() {

            @Override
            public AbstractPlayer getPlayer() {
                return player;
            }
        };
    }

    public static AbstractPlayer.PlayerClass getChosenClass(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getChosenClass();
    }

    public static void setChosenClass(AbstractPlayer item, AbstractPlayer.PlayerClass value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setChosenClass(value);
    }

    public static int getGameHandSize(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getGameHandSize();
    }

    public static void setGameHandSize(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setGameHandSize(value);
    }

    public static int getMasterHandSize(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getMasterHandSize();
    }

    public static void setMasterHandSize(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setMasterHandSize(value);
    }

    public static int getStartingMaxHP(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getStartingMaxHP();
    }

    public static void setStartingMaxHP(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setStartingMaxHP(value);
    }

    public static CardGroup getMasterDeck(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getMasterDeck();
    }

    public static void setMasterDeck(AbstractPlayer item, CardGroup value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setMasterDeck(value);
    }

    public static CardGroup getDrawPile(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getDrawPile();
    }

    public static void setDrawPile(AbstractPlayer item, CardGroup value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setDrawPile(value);
    }

    public static CardGroup getHand(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getHand();
    }

    public static void setHand(AbstractPlayer item, CardGroup value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setHand(value);
    }

    public static CardGroup getDiscardPile(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getDiscardPile();
    }

    public static void setDiscardPile(AbstractPlayer item, CardGroup value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setDiscardPile(value);
    }

    public static CardGroup getExhaustPile(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getExhaustPile();
    }

    public static void setExhaustPile(AbstractPlayer item, CardGroup value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setExhaustPile(value);
    }

    public static CardGroup getLimbo(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getLimbo();
    }

    public static void setLimbo(AbstractPlayer item, CardGroup value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setLimbo(value);
    }

    public static ArrayList<AbstractRelic> getRelics(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getRelics();
    }

    public static void setRelics(AbstractPlayer item, ArrayList<AbstractRelic> value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setRelics(value);
    }

    public static ArrayList<AbstractBlight> getBlights(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getBlights();
    }

    public static void setBlights(AbstractPlayer item, ArrayList<AbstractBlight> value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setBlights(value);
    }

    public static int getPotionSlots(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getPotionSlots();
    }

    public static void setPotionSlots(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setPotionSlots(value);
    }

    public static ArrayList<AbstractPotion> getPotions(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getPotions();
    }

    public static void setPotions(AbstractPlayer item, ArrayList<AbstractPotion> value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setPotions(value);
    }

    public static EnergyManager getEnergy(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getEnergy();
    }

    public static void setEnergy(AbstractPlayer item, EnergyManager value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setEnergy(value);
    }

    public static boolean getIsEndingTurn(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getIsEndingTurn();
    }

    public static void setIsEndingTurn(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setIsEndingTurn(value);
    }

    public static boolean getViewingRelics(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getViewingRelics();
    }

    public static void setViewingRelics(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setViewingRelics(value);
    }

    public static boolean getInspectMode(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getInspectMode();
    }

    public static void setInspectMode(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setInspectMode(value);
    }

    public static Hitbox getInspectHb(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getInspectHb();
    }

    public static void setInspectHb(AbstractPlayer item, Hitbox value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setInspectHb(value);
    }

    public static int getDamagedThisCombat(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getDamagedThisCombat();
    }

    public static void setDamagedThisCombat(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setDamagedThisCombat(value);
    }

    public static String getTitle(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getTitle();
    }

    public static void setTitle(AbstractPlayer item, String value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setTitle(value);
    }

    public static ArrayList<AbstractOrb> getOrbs(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getOrbs();
    }

    public static void setOrbs(AbstractPlayer item, ArrayList<AbstractOrb> value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setOrbs(value);
    }

    public static int getMasterMaxOrbs(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getMasterMaxOrbs();
    }

    public static void setMasterMaxOrbs(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setMasterMaxOrbs(value);
    }

    public static int getMaxOrbs(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getMaxOrbs();
    }

    public static void setMaxOrbs(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setMaxOrbs(value);
    }

    public static AbstractStance getStance(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getStance();
    }

    public static void setStance(AbstractPlayer item, AbstractStance value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setStance(value);
    }

    public static int getCardsPlayedThisTurn(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getCardsPlayedThisTurn();
    }

    public static void setCardsPlayedThisTurn(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setCardsPlayedThisTurn(value);
    }

    public static boolean getIsHoveringCard(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getIsHoveringCard();
    }

    public static void setIsHoveringCard(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setIsHoveringCard(value);
    }

    public static boolean getIsHoveringDropZone(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getIsHoveringDropZone();
    }

    public static void setIsHoveringDropZone(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setIsHoveringDropZone(value);
    }

    public static float getHoverStartLine(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getHoverStartLine();
    }

    public static void setHoverStartLine(AbstractPlayer item, float value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setHoverStartLine(value);
    }

    public static boolean getPassedHesitationLine(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getPassedHesitationLine();
    }

    public static void setPassedHesitationLine(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setPassedHesitationLine(value);
    }

    public static AbstractCard getHoveredCard(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getHoveredCard();
    }

    public static void setHoveredCard(AbstractPlayer item, AbstractCard value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setHoveredCard(value);
    }

    public static AbstractCard getToHover(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getToHover();
    }

    public static void setToHover(AbstractPlayer item, AbstractCard value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setToHover(value);
    }

    public static AbstractCard getCardInUse(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getCardInUse();
    }

    public static void setCardInUse(AbstractPlayer item, AbstractCard value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setCardInUse(value);
    }

    public static boolean getIsDraggingCard(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getIsDraggingCard();
    }

    public static void setIsDraggingCard(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setIsDraggingCard(value);
    }

    public static boolean getIsUsingClickDragControl(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getIsUsingClickDragControl();
    }

    public static void setIsUsingClickDragControl(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setIsUsingClickDragControl(value);
    }

    public static float getClickDragTimer(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getClickDragTimer();
    }

    public static void setClickDragTimer(AbstractPlayer item, float value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setClickDragTimer(value);
    }

    public static boolean getInSingleTargetMode(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getInSingleTargetMode();
    }

    public static void setInSingleTargetMode(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setInSingleTargetMode(value);
    }

    public static AbstractMonster getHoveredMonster(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getHoveredMonster();
    }

    public static void setHoveredMonster(AbstractPlayer item, AbstractMonster value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setHoveredMonster(value);
    }

    public static float getHoverEnemyWaitTimer(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getHoverEnemyWaitTimer();
    }

    public static void setHoverEnemyWaitTimer(AbstractPlayer item, float value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setHoverEnemyWaitTimer(value);
    }

    public static boolean getIsInKeyboardMode(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getIsInKeyboardMode();
    }

    public static void setIsInKeyboardMode(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setIsInKeyboardMode(value);
    }

    public static boolean getSkipMouseModeOnce(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getSkipMouseModeOnce();
    }

    public static void setSkipMouseModeOnce(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setSkipMouseModeOnce(value);
    }

    public static int getKeyboardCardIndex(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getKeyboardCardIndex();
    }

    public static void setKeyboardCardIndex(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setKeyboardCardIndex(value);
    }

    public static int getTouchscreenInspectCount(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getTouchscreenInspectCount();
    }

    public static void setTouchscreenInspectCount(AbstractPlayer item, int value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setTouchscreenInspectCount(value);
    }

    public static Texture getImg(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getImg();
    }

    public static void setImg(AbstractPlayer item, Texture value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setImg(value);
    }

    public static Texture getShoulderImg(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getShoulderImg();
    }

    public static void setShoulderImg(AbstractPlayer item, Texture value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setShoulderImg(value);
    }

    public static Texture getShoulder2Img(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getShoulder2Img();
    }

    public static void setShoulder2Img(AbstractPlayer item, Texture value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setShoulder2Img(value);
    }

    public static Texture getCorpseImg(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getCorpseImg();
    }

    public static void setCorpseImg(AbstractPlayer item, Texture value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setCorpseImg(value);
    }

    public static float getArrowScale(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getArrowScale();
    }

    public static void setArrowScale(AbstractPlayer item, float value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setArrowScale(value);
    }

    public static float getArrowScaleTimer(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getArrowScaleTimer();
    }

    public static void setArrowScaleTimer(AbstractPlayer item, float value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setArrowScaleTimer(value);
    }

    public static float getArrowX(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getArrowX();
    }

    public static void setArrowX(AbstractPlayer item, float value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setArrowX(value);
    }

    public static float getArrowY(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getArrowY();
    }

    public static void setArrowY(AbstractPlayer item, float value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setArrowY(value);
    }

    public static boolean getEndTurnQueued(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getEndTurnQueued();
    }

    public static void setEndTurnQueued(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setEndTurnQueued(value);
    }

    public static Vector2[] getPoints(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getPoints();
    }

    public static void setPoints(AbstractPlayer item, Vector2[] value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setPoints(value);
    }

    public static Vector2 getControlPoint(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getControlPoint();
    }

    public static void setControlPoint(AbstractPlayer item, Vector2 value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setControlPoint(value);
    }

    public static Vector2 getArrowTmp(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getArrowTmp();
    }

    public static void setArrowTmp(AbstractPlayer item, Vector2 value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setArrowTmp(value);
    }

    public static Vector2 getStartArrowVector(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getStartArrowVector();
    }

    public static void setStartArrowVector(AbstractPlayer item, Vector2 value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setStartArrowVector(value);
    }

    public static Vector2 getEndArrowVector(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getEndArrowVector();
    }

    public static void setEndArrowVector(AbstractPlayer item, Vector2 value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setEndArrowVector(value);
    }

    public static boolean getRenderCorpse(AbstractPlayer item) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        return handler.getRenderCorpse();
    }

    public static void setRenderCorpse(AbstractPlayer item, boolean value) {
        PlayerHandler<?> handler = item instanceof PlayerHandler ? (PlayerHandler<?>) item : PlayerFields.getDefaultPlayerHandler(item);
        handler.setRenderCorpse(value);
    }

    public static String getName(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getName();
    }

    public static void setName(AbstractCreature item, String value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setName(value);
    }

    public static String getId(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getId();
    }

    public static void setId(AbstractCreature item, String value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setId(value);
    }

    public static ArrayList<AbstractPower> getPowers(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getPowers();
    }

    public static void setPowers(AbstractCreature item, ArrayList<AbstractPower> value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setPowers(value);
    }

    public static boolean getIsPlayer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getIsPlayer();
    }

    public static void setIsPlayer(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setIsPlayer(value);
    }

    public static boolean getIsBloodied(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getIsBloodied();
    }

    public static void setIsBloodied(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setIsBloodied(value);
    }

    public static float getDrawX(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getDrawX();
    }

    public static void setDrawX(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setDrawX(value);
    }

    public static float getDrawY(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getDrawY();
    }

    public static void setDrawY(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setDrawY(value);
    }

    public static float getDialogX(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getDialogX();
    }

    public static void setDialogX(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setDialogX(value);
    }

    public static float getDialogY(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getDialogY();
    }

    public static void setDialogY(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setDialogY(value);
    }

    public static Hitbox getHb(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHb();
    }

    public static void setHb(AbstractCreature item, Hitbox value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHb(value);
    }

    public static int getGold(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getGold();
    }

    public static void setGold(AbstractCreature item, int value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setGold(value);
    }

    public static int getDisplayGold(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getDisplayGold();
    }

    public static void setDisplayGold(AbstractCreature item, int value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setDisplayGold(value);
    }

    public static boolean getIsDying(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getIsDying();
    }

    public static void setIsDying(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setIsDying(value);
    }

    public static boolean getIsDead(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getIsDead();
    }

    public static void setIsDead(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setIsDead(value);
    }

    public static boolean getHalfDead(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHalfDead();
    }

    public static void setHalfDead(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHalfDead(value);
    }

    public static boolean getFlipHorizontal(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getFlipHorizontal();
    }

    public static void setFlipHorizontal(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setFlipHorizontal(value);
    }

    public static boolean getFlipVertical(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getFlipVertical();
    }

    public static void setFlipVertical(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setFlipVertical(value);
    }

    public static float getEscapeTimer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getEscapeTimer();
    }

    public static void setEscapeTimer(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setEscapeTimer(value);
    }

    public static boolean getIsEscaping(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getIsEscaping();
    }

    public static void setIsEscaping(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setIsEscaping(value);
    }

    public static ArrayList<PowerTip> getTips(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getTips();
    }

    public static void setTips(AbstractCreature item, ArrayList<PowerTip> value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setTips(value);
    }

    public static Hitbox getHealthHb(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHealthHb();
    }

    public static void setHealthHb(AbstractCreature item, Hitbox value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHealthHb(value);
    }

    public static float getHealthHideTimer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHealthHideTimer();
    }

    public static void setHealthHideTimer(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHealthHideTimer(value);
    }

    public static int getLastDamageTaken(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getLastDamageTaken();
    }

    public static void setLastDamageTaken(AbstractCreature item, int value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setLastDamageTaken(value);
    }

    public static float getHbX(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbX();
    }

    public static void setHbX(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbX(value);
    }

    public static float getHbY(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbY();
    }

    public static void setHbY(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbY(value);
    }

    public static float getHbW(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbW();
    }

    public static void setHbW(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbW(value);
    }

    public static float getHbH(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbH();
    }

    public static void setHbH(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbH(value);
    }

    public static int getCurrentHealth(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getCurrentHealth();
    }

    public static void setCurrentHealth(AbstractCreature item, int value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setCurrentHealth(value);
    }

    public static int getMaxHealth(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getMaxHealth();
    }

    public static void setMaxHealth(AbstractCreature item, int value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setMaxHealth(value);
    }

    public static int getCurrentBlock(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getCurrentBlock();
    }

    public static void setCurrentBlock(AbstractCreature item, int value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setCurrentBlock(value);
    }

    public static float getHealthBarWidth(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHealthBarWidth();
    }

    public static void setHealthBarWidth(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHealthBarWidth(value);
    }

    public static float getTargetHealthBarWidth(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getTargetHealthBarWidth();
    }

    public static void setTargetHealthBarWidth(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setTargetHealthBarWidth(value);
    }

    public static float getHbShowTimer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbShowTimer();
    }

    public static void setHbShowTimer(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbShowTimer(value);
    }

    public static float getHealthBarAnimTimer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHealthBarAnimTimer();
    }

    public static void setHealthBarAnimTimer(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHealthBarAnimTimer(value);
    }

    public static float getBlockAnimTimer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getBlockAnimTimer();
    }

    public static void setBlockAnimTimer(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setBlockAnimTimer(value);
    }

    public static float getBlockOffset(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getBlockOffset();
    }

    public static void setBlockOffset(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setBlockOffset(value);
    }

    public static float getBlockScale(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getBlockScale();
    }

    public static void setBlockScale(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setBlockScale(value);
    }

    public static float getHbAlpha(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbAlpha();
    }

    public static void setHbAlpha(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbAlpha(value);
    }

    public static float getHbYOffset(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbYOffset();
    }

    public static void setHbYOffset(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbYOffset(value);
    }

    public static Color getHbBgColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbBgColor();
    }

    public static void setHbBgColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbBgColor(value);
    }

    public static Color getHbShadowColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbShadowColor();
    }

    public static void setHbShadowColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbShadowColor(value);
    }

    public static Color getBlockColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getBlockColor();
    }

    public static void setBlockColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setBlockColor(value);
    }

    public static Color getBlockOutlineColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getBlockOutlineColor();
    }

    public static void setBlockOutlineColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setBlockOutlineColor(value);
    }

    public static Color getBlockTextColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getBlockTextColor();
    }

    public static void setBlockTextColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setBlockTextColor(value);
    }

    public static Color getRedHbBarColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getRedHbBarColor();
    }

    public static void setRedHbBarColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setRedHbBarColor(value);
    }

    public static Color getGreenHbBarColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getGreenHbBarColor();
    }

    public static void setGreenHbBarColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setGreenHbBarColor(value);
    }

    public static Color getBlueHbBarColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getBlueHbBarColor();
    }

    public static void setBlueHbBarColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setBlueHbBarColor(value);
    }

    public static Color getOrangeHbBarColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getOrangeHbBarColor();
    }

    public static void setOrangeHbBarColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setOrangeHbBarColor(value);
    }

    public static Color getHbTextColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getHbTextColor();
    }

    public static void setHbTextColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setHbTextColor(value);
    }

    public static TintEffect getTint(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getTint();
    }

    public static void setTint(AbstractCreature item, TintEffect value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setTint(value);
    }

    public static boolean getShakeToggle(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getShakeToggle();
    }

    public static void setShakeToggle(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setShakeToggle(value);
    }

    public static float getAnimX(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getAnimX();
    }

    public static void setAnimX(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setAnimX(value);
    }

    public static float getAnimY(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getAnimY();
    }

    public static void setAnimY(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setAnimY(value);
    }

    public static float getVX(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getVX();
    }

    public static void setVX(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setVX(value);
    }

    public static float getVY(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getVY();
    }

    public static void setVY(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setVY(value);
    }

    public static AbstractCreature.CreatureAnimation getAnimation(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getAnimation();
    }

    public static void setAnimation(AbstractCreature item, AbstractCreature.CreatureAnimation value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setAnimation(value);
    }

    public static float getAnimationTimer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getAnimationTimer();
    }

    public static void setAnimationTimer(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setAnimationTimer(value);
    }

    public static TextureAtlas getAtlas(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getAtlas();
    }

    public static void setAtlas(AbstractCreature item, TextureAtlas value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setAtlas(value);
    }

    public static Skeleton getSkeleton(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getSkeleton();
    }

    public static void setSkeleton(AbstractCreature item, Skeleton value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setSkeleton(value);
    }

    public static AnimationState getState(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getState();
    }

    public static void setState(AbstractCreature item, AnimationState value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setState(value);
    }

    public static AnimationStateData getStateData(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getStateData();
    }

    public static void setStateData(AbstractCreature item, AnimationStateData value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setStateData(value);
    }

    public static float getReticleAlpha(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getReticleAlpha();
    }

    public static void setReticleAlpha(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setReticleAlpha(value);
    }

    public static Color getReticleColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getReticleColor();
    }

    public static void setReticleColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setReticleColor(value);
    }

    public static Color getReticleShadowColor(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getReticleShadowColor();
    }

    public static void setReticleShadowColor(AbstractCreature item, Color value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setReticleShadowColor(value);
    }

    public static boolean getReticleRendered(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getReticleRendered();
    }

    public static void setReticleRendered(AbstractCreature item, boolean value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setReticleRendered(value);
    }

    public static float getReticleOffset(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getReticleOffset();
    }

    public static void setReticleOffset(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setReticleOffset(value);
    }

    public static float getReticleAnimTimer(AbstractCreature item) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        return handler.getReticleAnimTimer();
    }

    public static void setReticleAnimTimer(AbstractCreature item, float value) {
        CreatureHandler<?> handler = item instanceof CreatureHandler ? (CreatureHandler<?>) item : PlayerFields.getDefaultCreatureHandler(item);
        handler.setReticleAnimTimer(value);
    }

    @SpirePatches2({ @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.AbstractGameAction", method = "isDeadOrEscaped", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.AbstractGameAction", method = "shouldCancelAction", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.ClearCardQueueAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "endTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "getNextAction", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "callEndOfTurnActions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "callEndTurnEarlySequence", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "cleanCardQueue", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "incrementDiscard", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.GameActionManager", method = "queueExtraCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.animations.SetAnimationAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.animations.ShoutAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.animations.TalkAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.ApplyPoisonOnRandomMonsterAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.ApplyPowerAction", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.megacrit.cardcrawl.core.AbstractCreature", "com.megacrit.cardcrawl.powers.AbstractPower", "int", "boolean", "com.megacrit.cardcrawl.actions.AbstractGameAction$AttackEffect" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.ApplyPowerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.AttackDamageRandomEnemyAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.BetterDiscardPileToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.BetterDrawPileToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DamageAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DamageAction", method = "stealGold", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DarkOrbEvokeAction", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "com.megacrit.cardcrawl.actions.AbstractGameAction$AttackEffect" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DarkOrbEvokeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DiscardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DiscardAtEndOfTurnAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DiscardSpecificCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.DrawCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.EmptyDeckShuffleAction", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.EmptyDeckShuffleAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.ExhaustAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.FastDrawCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.GainBlockAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.GainEnergyAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.GainEnergyAndEnableControlsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.InstantKillAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.LoseHPAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.LosePercentHPAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.MakeTempCardAtBottomOfDeckAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.MillAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.PlayTopCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.PummelDamageAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.PutOnBottomOfDeckAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.PutOnDeckAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.RemoveAllBlockAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.ReviveMonsterAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.ShowMoveNameAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.ShuffleAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.SpawnMonsterAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.SuicideAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.TransformCardInHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.common.UpgradeRandomCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.AggregateEnergyAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.AllCostToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.BarrageAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.BlasterAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.CacheAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ChannelAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.CompileDriverAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.DamageAllButOneEnemyAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.DarkImpulseAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.DiscardPileToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.EssenceOfDarknessAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.EvokeAllOrbsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.FluxAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.GashAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.IceWallAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ImpulseAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.IncreaseMiscAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.LightningOrbEvokeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.LightningOrbPassiveAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.NewRipAndTearAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.NewThunderStrikeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.RecycleAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.RedoAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ReinforcedBodyAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ReprieveAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ScrapeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ScrapeFollowUpAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.SeekAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ShuffleAllAction", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ShuffleAllAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.SunderAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.ThunderStrikeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.defect.TriggerEndOfTurnOrbsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.deprecated.DEPRECATEDBlockSelectedAmountAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.deprecated.DEPRECATEDBrillianceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.deprecated.DEPRECATEDDamagePerCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.deprecated.DEPRECATEDExperiencedAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.deprecated.DEPRECATEDRandomStanceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ApotheosisAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ApplyBulletTimeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ApplyStasisAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ArmamentsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ArmamentsAction", method = "returnCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.AttackFromDeckToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.BaneAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.BendAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.BendAction", method = "returnCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.BladeFuryAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.BlockPerNonAttackAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.BouncingFlaskAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.BurnIncreaseAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.CalculatedGambleAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ChannelDestructionAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.CrowReviveAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DamagePerAttackPlayedAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DeckToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DiscardPileToTopOfDeckAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DiscoveryAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DoppelgangerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DoubleYourBlockAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DualWieldAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.DualWieldAction", method = "returnCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.EnlightenmentAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.EstablishmentPowerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ExhaustAllNonAttackAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ExhumeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ExpertiseAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.FeedAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.FiendFireAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.FlechetteAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ForethoughtAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.FullHealthAdditionalDamageAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.GainBlockRandomMonsterAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.GainEnergyIfDiscardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.GamblingChipAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.GreedAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ImmolateAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.ImmolateAction", method = "dealDamage", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.IncreaseMaxHpAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.InspirationAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.MadnessAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.MadnessAction", method = "findAndModifyCard", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.MalaiseAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.MindBlastAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.MulticastAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.NightmareAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.PatientMissileAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.PoisonLoseHpAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RandomCardFromDiscardPileToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RandomizeHandCostAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RegenAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RemoveAllPowersAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RemoveDebuffsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RestoreRetainedCardsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RetainCardsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RipAndTearAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.RitualDaggerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SetupAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SkewerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SkillFromDeckToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SpawnDaggerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SpotWeaknessAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.StepThroughTimeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SummonGremlinAction", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster[]" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SummonGremlinAction", method = "identifySlot", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster[]" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SummonGremlinAction", method = "getSmartPosition", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SummonGremlinAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.SwordBoomerangAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.TempestAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.TransmutationAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.TransmuteAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.Transmutev2Action", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.TripleYourBlockAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.TumbleAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.UndoAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.UnloadAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.VampireDamageAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.VampireDamageAllEnemiesAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.unique.WhirlwindAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.ChooseOneColorless", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.ConditionalDrawAction", method = "checkCondition", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.DiscardToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.DrawPileToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.ExhaustAllEtherealAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.ExhaustToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.HandCheckAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.LoseBlockAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.ScryAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.ShowCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.ShowCardAndPoofAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.UnhoverCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.UnlimboAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.UseCardAction", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.utility.UseCardAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.BrillianceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.ChangeStanceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.ClarityAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.CollectAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.ConjureBladeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.ContemplateAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.ContemplateAction", method = "returnCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.DivinePunishmentAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.EmotionalTurmoilAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.EmptyBodyAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.ExpungeVFXAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.FlickerAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.FlickerReturnToHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.ForeignInfluenceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.HaltAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.IndignationAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.InnerPeaceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.JudgementAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.LessonLearnedAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.MasterRealityAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.MeditateAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.NotStanceCheckAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.OmniscienceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.PathVictoryAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.PerfectedFormAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.SanctityAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.SpiritShieldAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.StanceCheckAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.SwipeAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.TranscendenceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.TriggerMarksAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.UnravelingAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.VengeanceAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.actions.watcher.WallopAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.AbstractBlight", method = "obtain", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.AbstractBlight", method = "instantObtain", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "int", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.Durian", method = "stack", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.Durian", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.Scatterbrain", method = "stack", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.Scatterbrain", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.VoidEssence", method = "stack", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.blights.VoidEssence", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "cardPlayable", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "hasEnoughEnergy", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "renderImage", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "boolean", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "renderEnergy", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "renderCardTip", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "renderCardPreview", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "triggerOnEndOfPlayerTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "applyPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "applyPowersToBlock", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.AbstractCard", method = "calculateCardDamage", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "removeCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "refreshHandLayout", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "moveToExhaustPile", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "moveToHand", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.cards.CardGroup" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "moveToHand", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "resetCardBeforeMoving", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "initializeDeck", paramtypes = { "com.megacrit.cardcrawl.cards.CardGroup" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.CardGroup", method = "triggerOnOtherCardPlayed", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.DamageInfo", method = "applyPowers", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.DamageInfo", method = "applyEnemyPowersOnly", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "discard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "shuffle", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "onToDeck", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "boolean", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "onToBottomOfDeck", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "empower", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "obtain", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.Soul", method = "isCarryingCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.AutoShields", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.Claw", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.Hyperbeam", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.MeteorStrike", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.Reboot", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.Scrape", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.Stack", method = "applyPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.Sunder", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.blue.SweepingBeam", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.Bite", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.DeepBreath", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.Impatience", method = "shouldGlow", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.MindBlast", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.MindBlast", method = "applyPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.SecretTechnique", method = "canUse", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.SecretWeapon", method = "canUse", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.colorless.ThinkingAhead", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.curses.Clumsy", method = "triggerOnEndOfPlayerTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.curses.Regret", method = "triggerOnEndOfTurnForPlayingCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.deprecated.DEPRECATEDFlare", method = "triggerOnGlowCheck", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.deprecated.DEPRECATEDNothingness", method = "countCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.green.BouncingFlask", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.green.CripplingPoison", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.green.DaggerThrow", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.green.Flechettes", method = "applyPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.green.GrandFinale", method = "triggerOnGlowCheck", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.green.GrandFinale", method = "canUse", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.green.PiercingWail", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.DeusExMachina", method = "triggerWhenDrawn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.EmptyBody", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.EmptyFist", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.EmptyMind", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.Fasting", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.FlyingSleeves", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.Foresight", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.Judgement", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.JustLucky", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.PressurePoints", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.SignatureMove", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.SignatureMove", method = "canUse", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.SignatureMove", method = "triggerOnGlowCheck", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.SpiritShield", method = "applyPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.ThirdEye", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.purple.WreathOfFlame", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Anger", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Barricade", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.BloodForBlood", method = "makeCopy", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Bludgeon", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.BodySlam", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.BodySlam", method = "applyPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Carnage", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Clash", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Clash", method = "canUse", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Corruption", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Feed", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.FlameBarrier", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.HeavyBlade", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Hemokinesis", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Intimidate", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.IronWave", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.PerfectedStrike", method = "countCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Rage", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.SearingBlow", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.ThunderClap", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.red.Warcry", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.tempCards.Insight", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.cards.tempCards.ThroughViolence", method = "use", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "<ctor>", paramtypes = { "java.lang.String", "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "getSaveFilePath", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "dispose", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "adjustPotionPositions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "initializeClass", paramtypes = { "java.lang.String", "java.lang.String", "java.lang.String", "java.lang.String", "com.megacrit.cardcrawl.screens.CharSelectInfo", "float", "float", "float", "float", "com.megacrit.cardcrawl.core.EnergyManager" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "initializeStarterDeck", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "combatUpdate", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateViewRelicControls", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "loseGold", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "gainGold", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "playDeathAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "isCursed", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateSingleTargetInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateFullKeyboardCardSelection", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "hoverCardInHand", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateTargetArrowWithKeyboard", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderCardHotKeyText", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "clickAndDragCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "manuallySelectCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "playCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "releaseCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "onCardDrawOrDiscard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "useCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.monsters.AbstractMonster", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateCardsOnDamage", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateCardsOnDiscard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "heal", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "gainEnergy", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "preBattlePrep", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "getRelicNames", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "getCircletCount", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "draw", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "draw", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderShoulderImg", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderPlayerImage", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderPlayerBattleUi", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderPowerTips", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderHand", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderTargetingUi", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "drawCurvedLine", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "com.badlogic.gdx.math.Vector2", "com.badlogic.gdx.math.Vector2", "com.badlogic.gdx.math.Vector2" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "createHandIsFullDialog", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderHoverReticle", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyPreCombatLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyStartOfCombatLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyStartOfCombatPreDrawLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyStartOfTurnRelics", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyStartOfTurnPostDrawRelics", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyStartOfTurnPreDrawCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyStartOfTurnCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "onVictory", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "hasRelic", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "hasBlight", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "hasPotion", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "hasAnyPotions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "loseRandomRelics", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "loseRelic", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "reorganizeRelics", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "getRelic", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "getBlight", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "obtainPotion", paramtypes = { "int", "com.megacrit.cardcrawl.potions.AbstractPotion" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "obtainPotion", paramtypes = { "com.megacrit.cardcrawl.potions.AbstractPotion" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderRelics", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "renderBlights", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "triggerEvokeAnimation", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "evokeOrb", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "evokeNewestOrb", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "evokeWithoutLosingOrb", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "removeNextOrb", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "hasEmptyOrb", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "hasOrb", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "filledOrbCount", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "channelOrb", paramtypes = { "com.megacrit.cardcrawl.orbs.AbstractOrb" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "increaseMaxOrbSlots", paramtypes = { "int", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "decreaseMaxOrbSlots", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "applyStartOfTurnOrbs", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "updateEscapeAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "relicsDoneAnimating", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "resetControllerValues", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "getRandomPotion", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "removePotion", paramtypes = { "com.megacrit.cardcrawl.potions.AbstractPotion" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "movePosition", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.AbstractPlayer", method = "switchedStance", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.CharacterManager", method = "setChosenCharacter", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.CharacterManager", method = "recreateCharacter", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.CharacterManager", method = "getCharacter", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Defect", method = "<ctor>", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Defect", method = "newInstance", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Defect", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Ironclad", method = "<ctor>", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Ironclad", method = "newInstance", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Ironclad", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.TheSilent", method = "<ctor>", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.TheSilent", method = "newInstance", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.TheSilent", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Watcher", method = "<ctor>", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Watcher", method = "newInstance", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Watcher", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.characters.Watcher", method = "renderPlayerImage", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "brokeBlock", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "decrementBlock", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "increaseMaxHp", paramtypes = { "int", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "decreaseMaxHealth", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "refreshHitboxLocation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateAnimations", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateFastAttackAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateSlowAttackAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateFastShakeAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateHopAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateJumpAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateStaggerAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateShakeAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "loadAnimation", paramtypes = { "java.lang.String", "java.lang.String", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "heal", paramtypes = { "int", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "addBlock", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "loseBlock", paramtypes = { "int", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "loseBlock", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "loseBlock", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "showHealthBar", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "hideHealthBar", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "addPower", paramtypes = { "com.megacrit.cardcrawl.powers.AbstractPower" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "applyStartOfTurnPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "applyTurnPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "applyStartOfTurnPostDrawPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "applyEndOfTurnTriggers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "healthBarUpdatedEvent", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "healthBarRevivedEvent", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateHbHoverFade", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateHbAlpha", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "gainBlockAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateBlockAnimations", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateHbPopInAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateHbDamageAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updatePowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderPowerTips", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "useFastAttackAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "useSlowAttackAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "useHopAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "useJumpAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "useStaggerAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "useFastShakeAnimation", paramtypes = { "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "useShakeAnimation", paramtypes = { "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "getPower", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "hasPower", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "isDeadOrEscaped", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "loseGold", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "gainGold", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderReticle", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderReticle", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "com.megacrit.cardcrawl.helpers.Hitbox" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "updateReticle", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderHealth", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderBlockOutline", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderBlockIconAndValue", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderHealthBg", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderOrangeHealthBar", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderGreenHealthBar", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderRedHealthBar", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderHealthText", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderPowerIcons", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderReticleCorner", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float", "com.megacrit.cardcrawl.helpers.Hitbox", "boolean", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.AbstractCreature", method = "renderReticleCorner", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float", "boolean", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.CardCrawlGame", method = "doorUnlockScreenCheck", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.CardCrawlGame", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.CardCrawlGame", method = "setupTrialPlayer", paramtypes = { "com.megacrit.cardcrawl.trials.AbstractTrial" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.CardCrawlGame", method = "loadPlayerSave", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.CardCrawlGame", method = "createCharacter", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.CardCrawlGame", method = "updateDebugSwitch", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.CardCrawlGame", method = "isInARun", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.OverlayMenu", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.OverlayMenu", method = "showCombatPanels", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.OverlayMenu", method = "hideCombatPanels", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.core.OverlayMenu", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.daily.DailyScreen", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.daily.DailyScreen", method = "determineLoadout", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "loadSeeds", paramtypes = { "com.megacrit.cardcrawl.saveAndContinue.SaveFile" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "initializePotions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "initializeRelicList", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "getColorlessRewardCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "getRewardCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "resetPlayer", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "nextRoomTransition", paramtypes = { "com.megacrit.cardcrawl.saveAndContinue.SaveFile" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "incrementFloorBasedMetrics", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "getShrine", paramtypes = { "com.megacrit.cardcrawl.random.Random" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "getEvent", paramtypes = { "com.megacrit.cardcrawl.random.Random" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "closeCurrentScreen", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "genericScreenOverlayReset", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "dungeonTransitionSetup", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "reset", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "onModifyPower", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.dungeons.AbstractDungeon", method = "checkForPactAchievement", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.AbstractImageEvent", method = "enterCombatFromImage", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.GenericEventDialog", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.RoomEventDialog", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.Falling", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.MindBloom", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.MoaiHead", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.MoaiHead", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.SpireHeart", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.TombRedMask", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.TombRedMask", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.beyond.WindingHalls", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Addict", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Addict", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.BackToBasics", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.BackToBasics", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.BackToBasics", method = "upgradeStrikeAndDefends", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Beggar", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Beggar", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Beggar", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Colosseum", method = "reopen", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.DrugDealer", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.DrugDealer", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.DrugDealer", method = "transform", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.ForgottenAltar", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.ForgottenAltar", method = "gainChalice", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Ghosts", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.MaskedBandits", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.MaskedBandits", method = "stealGold", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.TheLibrary", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.TheLibrary", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Vampires", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.city.Vampires", method = "replaceAttacks", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.BigFish", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.Cleric", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.Cleric", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.Cleric", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.GoldenIdolEvent", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.GoldenWing", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.GoldenWing", method = "purgeLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.GoopPuddle", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.GoopPuddle", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.LivingWall", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.LivingWall", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.LivingWall", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.Mushrooms", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.Mushrooms", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.ShiningLight", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.ShiningLight", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.exordium.ShiningLight", method = "upgradeCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.AccursedBlacksmith", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.AccursedBlacksmith", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Bonfire", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Bonfire", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Bonfire", method = "setReward", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard$CardRarity" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Designer", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Designer", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Designer", method = "upgradeTwoRandomCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Duplicator", method = "use", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.FaceTrader", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.FountainOfCurseRemoval", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.GremlinMatchGame", method = "initializeCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.GremlinWheelGame", method = "preApplyResult", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.GremlinWheelGame", method = "applyResult", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.GremlinWheelGame", method = "purgeLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Nloth", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.NoteForYourself", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.NoteForYourself", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.PurificationShrine", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.PurificationShrine", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Transmogrifier", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.Transmogrifier", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.UpgradeShrine", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.UpgradeShrine", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.WeMeetAgain", method = "getRandomNonBasicCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.WeMeetAgain", method = "getGoldAmount", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.WeMeetAgain", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.WomanInBlue", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.events.shrines.WomanInBlue", method = "buttonEffect", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.BlightHelper", method = "getRandomBlight", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.CardHelper", method = "hasCardWithXDamage", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.CardHelper", method = "hasCardWithID", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.CardHelper", method = "hasCardType", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard$CardType" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.CardHelper", method = "hasCardWithType", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard$CardType" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.CardHelper", method = "returnCardOfType", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard$CardType", "com.megacrit.cardcrawl.random.Random" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.CardHelper", method = "hasUpgradedCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.CardHelper", method = "returnUpgradedCard", paramtypes = { "com.megacrit.cardcrawl.random.Random" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.EffectHelper", method = "gainGold", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "float", "float", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.GetAllInBattleInstances", method = "get", paramtypes = { "java.util.UUID" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.RelicLibrary", method = "addClassSpecificRelics", paramtypes = { "java.util.ArrayList" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.TipHelper", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.controller.CInputHelper", method = "isTopPanelActive", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.input.InputHelper", method = "updateFirst", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.helpers.input.InputHelper", method = "leaveControllerMode", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.metrics.Metrics", method = "gatherAllData", paramtypes = { "boolean", "boolean", "com.megacrit.cardcrawl.monsters.MonsterGroup" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.metrics.Metrics", method = "gatherAllDataAndSave", paramtypes = { "boolean", "boolean", "com.megacrit.cardcrawl.monsters.MonsterGroup" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "<ctor>", paramtypes = { "java.lang.String", "java.lang.String", "int", "float", "float", "float", "float", "java.lang.String", "float", "float", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "refreshIntentHbLocation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "unhover", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "updateIntent", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "renderTip", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "heal", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "setHp", paramtypes = { "int", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "renderName", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "updateHitbox", paramtypes = { "float", "float", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "updateDeathAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "dispose", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "updateEscapeAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "escape", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "die", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "useUniversalPreBattleAction", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "calculateDamage", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "applyBackAttack", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "onBossVictoryLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "onFinalBossVictoryLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "getLocStrings", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.AbstractMonster", method = "lambda$static$0", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "areMonstersDead", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "areMonstersBasicallyDead", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "applyPreTurnLogic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "getMonster", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "queueMonsters", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "hasMonsterEscaped", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "getRandomMonster", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster", "boolean", "com.megacrit.cardcrawl.random.Random" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "getRandomMonster", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "shouldFlipVfx", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "applyEndOfTurnPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "renderReticle", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.MonsterGroup", method = "getMonsterNames", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.AwakenedOne", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.AwakenedOne", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.AwakenedOne", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.AwakenedOne", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.AwakenedOne", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.AwakenedOne", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.AwakenedOne", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Darkling", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Darkling", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Darkling", method = "getMove", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Darkling", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Darkling", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Deca", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Deca", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Deca", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Donu", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Donu", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Donu", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Exploder", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.GiantHead", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Maw", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Maw", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Nemesis", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Nemesis", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Nemesis", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Nemesis", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Nemesis", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.OrbWalker", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.OrbWalker", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.OrbWalker", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Reptomancer", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Reptomancer", method = "usePreBattleAction", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Reptomancer", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Reptomancer", method = "canSpawn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Reptomancer", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Reptomancer", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Reptomancer", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Repulsor", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.SnakeDagger", method = "initializeAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.SnakeDagger", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.SnakeDagger", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.SnakeDagger", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Spiker", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.SpireGrowth", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.SpireGrowth", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.SpireGrowth", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.TimeEater", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.TimeEater", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.TimeEater", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.TimeEater", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.TimeEater", method = "getMove", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Transient", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Transient", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.Transient", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.WrithingMass", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.WrithingMass", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.beyond.WrithingMass", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditBear", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditBear", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditBear", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditBear", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditLeader", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditLeader", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditLeader", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditLeader", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditPointy", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditPointy", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BanditPointy", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BookOfStabbing", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BookOfStabbing", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BookOfStabbing", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BronzeAutomaton", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BronzeAutomaton", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BronzeAutomaton", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BronzeOrb", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.BronzeOrb", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Byrd", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Byrd", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Centurion", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Centurion", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Centurion", method = "getMove", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Centurion", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Centurion", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Champ", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Champ", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Champ", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Champ", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Champ", method = "getMove", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Chosen", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Chosen", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Chosen", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.GremlinLeader", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.GremlinLeader", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.GremlinLeader", method = "numAliveGremlins", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.GremlinLeader", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.GremlinLeader", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.GremlinLeader", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Healer", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Healer", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Healer", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Healer", method = "getMove", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Healer", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Healer", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Mugger", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Mugger", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Mugger", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.ShelledParasite", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.ShelledParasite", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.ShelledParasite", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.ShelledParasite", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.SnakePlant", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.SnakePlant", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.SnakePlant", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.SnakePlant", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Snecko", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Snecko", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Snecko", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Snecko", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.SphericGuardian", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.SphericGuardian", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.SphericGuardian", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.Taskmaster", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.TheCollector", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.TheCollector", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.TheCollector", method = "isMinionDead", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.TheCollector", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.TheCollector", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.TorchHead", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.city.TorchHead", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.CorruptHeart", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.CorruptHeart", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.CorruptHeart", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireShield", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireShield", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireShield", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireShield", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireShield", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireSpear", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireSpear", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireSpear", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.ending.SpireSpear", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.AcidSlime_L", method = "<ctor>", paramtypes = { "float", "float", "int", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.AcidSlime_L", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.AcidSlime_L", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.AcidSlime_M", method = "<ctor>", paramtypes = { "float", "float", "int", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.AcidSlime_S", method = "<ctor>", paramtypes = { "float", "float", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.ApologySlime", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Cultist", method = "<ctor>", paramtypes = { "float", "float", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Cultist", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.FungiBeast", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.FungiBeast", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.FungiBeast", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinFat", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinFat", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinFat", method = "escapeNext", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinFat", method = "deathReact", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinNob", method = "<ctor>", paramtypes = { "float", "float", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinThief", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinThief", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinThief", method = "escapeNext", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinThief", method = "deathReact", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinTsundere", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinTsundere", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinTsundere", method = "escapeNext", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinTsundere", method = "deathReact", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWarrior", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWarrior", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWarrior", method = "escapeNext", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWarrior", method = "deathReact", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWizard", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWizard", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWizard", method = "escapeNext", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.GremlinWizard", method = "deathReact", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Hexaghost", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Hexaghost", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Hexaghost", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.HexaghostBody", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.JawWorm", method = "<ctor>", paramtypes = { "float", "float", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.JawWorm", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Lagavulin", method = "<ctor>", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Lagavulin", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Lagavulin", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Looter", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Looter", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Looter", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.LouseDefensive", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.LouseDefensive", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.LouseDefensive", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.LouseNormal", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.LouseNormal", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Sentry", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Sentry", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Sentry", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.Sentry", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlaverBlue", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlaverRed", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlaverRed", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlaverRed", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlimeBoss", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlimeBoss", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlimeBoss", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SlimeBoss", method = "die", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SpikeSlime_L", method = "<ctor>", paramtypes = { "float", "float", "int", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SpikeSlime_L", method = "takeTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SpikeSlime_L", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SpikeSlime_M", method = "<ctor>", paramtypes = { "float", "float", "int", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.SpikeSlime_S", method = "<ctor>", paramtypes = { "float", "float", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.TheGuardian", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.TheGuardian", method = "changeState", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.monsters.exordium.TheGuardian", method = "damage", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.neow.NeowEvent", method = "<ctor>", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.neow.NeowEvent", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.neow.NeowReward", method = "<ctor>", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.neow.NeowReward", method = "<ctor>", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.neow.NeowReward", method = "getRewardDrawbackOptions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.neow.NeowReward", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.neow.NeowReward", method = "activate", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.orbs.AbstractOrb", method = "updateAnimation", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.orbs.AbstractOrb", method = "setSlot", paramtypes = { "int", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.orbs.Dark", method = "onEndOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.orbs.EmptyOrbSlot", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.orbs.Frost", method = "onEndOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.orbs.Lightning", method = "onEndOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.orbs.Lightning", method = "triggerPassiveEffect", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.BloodPotion", method = "use", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.DexterityPotion", method = "use", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.EntropicBrew", method = "use", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.EntropicBrew", method = "getPotency", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.ExplosivePotion", method = "use", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.FairyPotion", method = "use", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.GamblersBrew", method = "use", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.potions.SmokeBomb", method = "use", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.AccuracyPower", method = "updateExistingShivs", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.AccuracyPower", method = "onDrawOrDiscard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.AmplifyPower", method = "onUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.BackAttackPower", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.BarricadePower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.BurstPower", method = "onUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.CorpseExplosionPower", method = "onDeath", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.CurlUpPower", method = "onAttacked", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.DoubleTapPower", method = "onUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.DrawPower", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.DrawPower", method = "onRemove", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.DrawReductionPower", method = "onInitialApplication", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.DrawReductionPower", method = "onRemove", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.DuplicationPower", method = "onUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.EchoPower", method = "onUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.EquilibriumPower", method = "atEndOfTurn", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.ExplosivePower", method = "duringTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.FadingPower", method = "duringTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.FlightPower", method = "onAttacked", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.LoopPower", method = "atStartOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.MalleablePower", method = "atEndOfTurn", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.MalleablePower", method = "atEndOfRound", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.MalleablePower", method = "onAttacked", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.MetallicizePower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.NextTurnBlockPower", method = "atStartOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.NoxiousFumesPower", method = "atStartOfTurnPostDraw", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.PlatedArmorPower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.PlatedArmorPower", method = "onRemove", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.PoisonPower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.PoisonPower", method = "stackPower", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.ReactivePower", method = "onAttacked", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.RegenerateMonsterPower", method = "atEndOfTurn", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.RepairPower", method = "onVictory", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.RetainCardPower", method = "atEndOfTurn", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.SlowPower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.SplitPower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.StasisPower", method = "onDeath", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.StrikeUpPower", method = "updateExistingStrikes", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.StrikeUpPower", method = "onDrawOrDiscard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.ThieveryPower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.TimeMazePower", method = "onAfterUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.VulnerablePower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.VulnerablePower", method = "atDamageReceive", paramtypes = { "float", "com.megacrit.cardcrawl.cards.DamageInfo$DamageType" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.WeakPower", method = "updateDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.WeakPower", method = "atDamageGive", paramtypes = { "float", "com.megacrit.cardcrawl.cards.DamageInfo$DamageType" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.WinterPower", method = "atStartOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.deprecated.DEPRECATEDSerenityPower", method = "onAttacked", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.watcher.EndTurnDeathPower", method = "atStartOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.watcher.ForesightPower", method = "atStartOfTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.watcher.LikeWaterPower", method = "atEndOfTurnPreEndTurnCards", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.powers.watcher.OmegaPower", method = "atEndOfTurn", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "reorganizeObtain", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "int", "boolean", "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "instantObtain", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "int", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "instantObtain", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "obtain", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "getColumn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "updateRelicPopupClick", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AbstractRelic", method = "updateOffsetX", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.AncientTeaSet", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.ArtOfWar", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Astrolabe", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Astrolabe", method = "giveCards", paramtypes = { "java.util.ArrayList" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BlackBlood", method = "onVictory", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledFlame", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledFlame", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledFlame", method = "canSpawn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledLightning", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledLightning", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledLightning", method = "canSpawn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledTornado", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BottledTornado", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BurningBlood", method = "onVictory", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BustedCrown", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BustedCrown", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.BustedCrown", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CharonsAshes", method = "onExhaust", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CloakClasp", method = "onPlayerEndTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CoffeeDripper", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CoffeeDripper", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CoffeeDripper", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CursedKey", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CursedKey", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.CursedKey", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.DollysMirror", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.DuVuDoll", method = "onMasterDeckChange", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.DuVuDoll", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Ectoplasm", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Ectoplasm", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Ectoplasm", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.EmptyCage", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.EmptyCage", method = "deleteCards", paramtypes = { "java.util.ArrayList" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.EternalFeather", method = "onEnterRoom", paramtypes = { "com.megacrit.cardcrawl.rooms.AbstractRoom" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.FusionHammer", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.FusionHammer", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.FusionHammer", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Girya", method = "canSpawn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.GremlinHorn", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.GremlinHorn", method = "onMonsterDeath", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.HappyFlower", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Lantern", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.LizardTail", method = "onTrigger", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.MarkOfPain", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.MarkOfPain", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.MeatOnTheBone", method = "onTrigger", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.MummifiedHand", method = "onUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Necronomicon", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.NeowsLament", method = "atBattleStart", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Nunchaku", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Orichalcum", method = "onPlayerEndTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Orichalcum", method = "atTurnStart", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PandorasBox", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PeacePipe", method = "canSpawn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PeacePipe", method = "addCampfireOption", paramtypes = { "java.util.ArrayList" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PenNib", method = "onUseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.actions.utility.UseCardAction" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PenNib", method = "atBattleStart", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PhilosopherStone", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PhilosopherStone", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PotionBelt", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PreservedInsect", method = "atBattleStart", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.PrismaticShard", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.RedSkull", method = "onBloodied", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.RedSkull", method = "onNotBloodied", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.RingOfTheSerpent", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.RingOfTheSerpent", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.RunicDome", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.RunicDome", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.RunicDome", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.SacredBark", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Shovel", method = "canSpawn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.SlaversCollar", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.SlaversCollar", method = "beforeEnergyPrep", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.SlaversCollar", method = "onVictory", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.SneckoEye", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.SneckoEye", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Sozu", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Sozu", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Sozu", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Sundial", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Test1", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Test5", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Test6", method = "onPlayerEndTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Test6", method = "hasEnoughGold", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.TinyHouse", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.TwistedFunnel", method = "atBattleStart", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.UnceasingTop", method = "onRefreshHand", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.VelvetChoker", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.VelvetChoker", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.VelvetChoker", method = "onUnequip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Waffle", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.WarPaint", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.Whetstone", method = "onEquip", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.deprecated.DEPRECATEDDodecahedron", method = "getUpdatedDescription", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.relics.deprecated.DEPRECATEDDodecahedron", method = "isActive", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rewards.RewardItem", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard$CardColor" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.rewards.chests.AbstractChest", method = "open", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.rewards.chests.BossChest", method = "open", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "getCardRarity", paramtypes = { "int", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "alterCardRarityProbabilities", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "endTurn", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "endBattle", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "renderAboveTopPanel", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "applyEndOfTurnRelics", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "applyEndOfTurnPreCardPowers", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.AbstractRoom", method = "eventControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.CampfireUI", method = "initializeButtons", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.CampfireUI", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.CampfireUI", method = "updateCharacterPosition", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.CampfireUI", method = "getCampMessage", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.RestRoom", method = "onPlayerEntry", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.ShopRoom", method = "updatePurge", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.rooms.TrueVictoryRoom", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue", method = "saveExistsAndNotCorrupted", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue", method = "save", paramtypes = { "com.megacrit.cardcrawl.saveAndContinue.SaveFile" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.saveAndContinue.SaveFile", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.saveAndContinue.SaveFile$SaveType" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.CardRewardScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.CombatRewardScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DeathScreen", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.monsters.MonsterGroup" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DeathScreen", method = "getDeathText", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DeathScreen", method = "reopen", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DeathScreen", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DeathScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DiscardPileViewScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DiscardPileViewScreen", method = "updatePositions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DiscardPileViewScreen", method = "updateScrolling", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DiscardPileViewScreen", method = "calculateScrollBounds", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DiscardPileViewScreen", method = "open", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DiscardPileViewScreen", method = "hideCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DiscardPileViewScreen", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DrawPileViewScreen", method = "open", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.DungeonMapScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.ExhaustPileViewScreen", method = "open", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.GameOverScreen", method = "calculateUnlockProgress", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.GameOverScreen", method = "checkScoreBonus", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckSortHeader", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckViewScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckViewScreen", method = "updatePositions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckViewScreen", method = "updateScrolling", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckViewScreen", method = "updateClicking", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckViewScreen", method = "calculateScrollBounds", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckViewScreen", method = "hideCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.MasterDeckViewScreen", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.VictoryScreen", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.monsters.MonsterGroup" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.VictoryScreen", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.VictoryScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.VictoryScreen", method = "updateVfx", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.VictoryScreen", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.charSelect.CharacterOption", method = "<ctor>", paramtypes = { "java.lang.String", "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.badlogic.gdx.graphics.Texture", "com.badlogic.gdx.graphics.Texture" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.charSelect.CharacterOption", method = "<ctor>", paramtypes = { "java.lang.String", "com.megacrit.cardcrawl.characters.AbstractPlayer", "java.lang.String", "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.charSelect.CharacterOption", method = "updateHitbox", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.charSelect.CharacterOption", method = "renderInfo", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.charSelect.CharacterOption", method = "renderRelics", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.charSelect.CharacterSelectScreen", method = "updateButtons", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.custom.CustomModeCharacterButton", method = "updateHitbox", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.custom.CustomModeScreen", method = "updateEmbarkButton", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.mainMenu.MenuButton", method = "resumeGame", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.options.ConfirmPopup", method = "abandonRunFromMainMenu", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.options.ConfirmPopup", method = "yesButtonEffect", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.options.OptionsPanel", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.select.BossRelicSelectScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.select.HandCardSelectScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.select.HandCardSelectScreen", method = "selectHoveredCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.select.HandCardSelectScreen", method = "updateSelectedCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.select.HandCardSelectScreen", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.select.HandCardSelectScreen", method = "prep", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.screens.stats.CharStat", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.Merchant", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "initCards", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "updatePurge", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "purchaseCard", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "updatePurgeCard", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "purchasePurge", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "renderCardsAndPrices", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.ShopScreen", method = "renderPurge", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.StorePotion", method = "update", paramtypes = { "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.StorePotion", method = "purchasePotion", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.StorePotion", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.StoreRelic", method = "update", paramtypes = { "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.StoreRelic", method = "purchaseRelic", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.shop.StoreRelic", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.stances.AbstractStance", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.stances.DivinityStance", method = "onEnterStance", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.stances.WrathStance", method = "onEnterStance", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.trials.CustomTrial", method = "setupPlayer", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.trials.OneHpTrial", method = "setupPlayer", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.trials.TestTrial", method = "setupPlayer", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.FtueTip", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.buttons.EndTurnButton", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.buttons.EndTurnButton", method = "showWarning", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.buttons.EndTurnButton", method = "disable", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.buttons.EndTurnButton", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.campfire.RestOption", method = "<ctor>", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.DiscardPilePanel", method = "updatePositions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.DiscardPilePanel", method = "openDiscardPile", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.DiscardPilePanel", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.DrawPilePanel", method = "updatePositions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.DrawPilePanel", method = "openDrawPile", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.DrawPilePanel", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.EnergyPanel", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.ExhaustPanel", method = "updatePositions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.ExhaustPanel", method = "openExhaustPile", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.ExhaustPanel", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.PotionPopUp", method = "updateControllerTargetInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.PotionPopUp", method = "updateTargetMode", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.PotionPopUp", method = "updateInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "setPlayerName", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "unhoverHitboxes", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "updateControllerInput", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "controllerViewRelics", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "updateGold", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "updatePotions", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "updateRelics", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "adjustRelicHbs", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "destroyPotion", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "renderPotionTips", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "renderControllerUi", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "renderHP", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "renderPotions", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "renderRelics", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "renderGold", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.ui.panels.TopPanel", method = "renderTopRightIcons", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.unlock.misc.DefectUnlock", method = "onUnlockScreenOpen", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.unlock.misc.TheSilentUnlock", method = "onUnlockScreenOpen", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.unlock.misc.WatcherUnlock", method = "onUnlockScreenOpen", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.FastCardObtainEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.GainGoldTextEffect", method = "<ctor>", paramtypes = { "int" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.GainPennyEffect", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.GainPennyEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.NecronomicurseEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.PlayerTurnEffect", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.PlayerTurnEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.RainbowCardEffect", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.RelicAboveCreatureEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.SpotlightPlayerEffect", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.TouchPickupGold", method = "<ctor>", paramtypes = { "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireSleepEffect", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireSleepEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireSmithEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.campfire.CampfireTokeEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ExhaustCardEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDiscardEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDiscardEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDrawPileEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "float", "float", "boolean", "boolean", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDrawPileEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "boolean", "boolean" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToHandEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToHandEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.DaggerSprayEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.FlashIntentEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.FlashPowerEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.powers.AbstractPower" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.FlyingOrbEffect", method = "<ctor>", paramtypes = { "float", "float" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.HemokinesisEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.InflameEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.MiracleEffect", method = "<ctor>", paramtypes = { "com.badlogic.gdx.graphics.Color", "com.badlogic.gdx.graphics.Color", "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.MiracleEffect", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.OfferingEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.PowerIconShowEffect", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.powers.AbstractPower" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.ReaperEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.combat.WebEffect", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.stance.CalmParticleEffect", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.stance.DivinityParticleEffect", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.stance.StanceAuraEffect", method = "<ctor>", paramtypes = { "java.lang.String" }), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.stance.WrathParticleEffect", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.megacrit.cardcrawl.vfx.stance.WrathStanceChangeParticle", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "basemod.abstracts.CustomMonster", method = "render", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "<ctor>", paramtypes = { "java.lang.String", "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass", "com.megacrit.cardcrawl.ui.panels.energyorb.EnergyOrbInterface", "basemod.animations.AbstractAnimation" }), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "renderPlayerImage", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "getSaveFilePath", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "loadPrefs", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "getUnlockedCardCount", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "getWinStreakKey", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "getLeaderboardWinStreakKey", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "getCustomModeCharacterButtonImage", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "movePosition", paramtypes = { "float", "float" }), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "updateEscapeAnimation", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.CustomPlayer", method = "getPortraitImageName", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.events.PhasedEvent", method = "enterCombat", paramtypes = {}), @SpirePatch2(cls = "basemod.abstracts.events.phases.CombatPhase", method = "transition", paramtypes = { "basemod.abstracts.events.PhasedEvent" }), @SpirePatch2(cls = "basemod.animations.AbstractAnimation", method = "renderSprite", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "basemod.animations.G3DJAnimation", method = "renderModel", paramtypes = { "com.badlogic.gdx.graphics.g3d.ModelBatch", "com.badlogic.gdx.graphics.g3d.Environment" }), @SpirePatch2(cls = "basemod.BaseMod", method = "isBaseGameCharacter", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "basemod.BaseMod", method = "isBaseGameCharacter", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "basemod.BaseMod", method = "autoCalculateMonsterName", paramtypes = { "basemod.BaseMod$GetMonsterGroup" }), @SpirePatch2(cls = "basemod.BaseMod", method = "getMaxUnlockLevel", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "basemod.BaseMod", method = "findCharacter", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "basemod.BaseMod", method = "generateCharacterOptions", paramtypes = {}), @SpirePatch2(cls = "basemod.BaseMod", method = "generateCustomCharacterOptions", paramtypes = {}), @SpirePatch2(cls = "basemod.BaseMod", method = "publishAddCustomModeMods", paramtypes = { "java.util.List" }), @SpirePatch2(cls = "basemod.ConsoleTargetedPower", method = "updateTargetMode", paramtypes = {}), @SpirePatch2(cls = "basemod.ConsoleTargetedPower", method = "renderTargetingUi", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "basemod.devcommands.act.ActCommand", method = "prepareTransition", paramtypes = {}), @SpirePatch2(cls = "basemod.devcommands.blight.BlightRemove", method = "execute", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.blight.BlightRemove", method = "extraOptions", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.deck.DeckRemove", method = "execute", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.deck.DeckRemove", method = "extraOptions", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.gold.Gold", method = "execute", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.hand.HandDiscard", method = "execute", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.hand.HandDiscard", method = "extraOptions", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.hand.HandRemove", method = "execute", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.hand.HandRemove", method = "extraOptions", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.hand.HandSet", method = "execute", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.hand.HandSet", method = "extraOptions", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.history.History", method = "setLoadout", paramtypes = { "com.megacrit.cardcrawl.screens.stats.RunData" }), @SpirePatch2(cls = "basemod.devcommands.history.History", method = "characterIndex", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "basemod.devcommands.potions.Potions", method = "execute", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.potions.Potions", method = "extraOptions", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.devcommands.relic.RelicRemove", method = "extraOptions", paramtypes = { "java.lang.String[]", "int" }), @SpirePatch2(cls = "basemod.eventUtil.util.ConditionalEvent", method = "isValid", paramtypes = {}), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.actions.unique.DiscoveryAction.MoreThanTwoFix", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.actions.unique.DiscoveryAction" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.AlternateCardCosts", method = "modifiers", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.CardModifierPatches$CardModifierAtEndOfTurn", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.CardModifierPatches$CardModifierOnUseCard", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.actions.utility.UseCardAction", "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.DynamicTextBlocks", method = "unwrap", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "java.lang.String" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.MultiCardPreviewRenderer$RenderMultiCardPreviewPatch", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.characters.AbstractPlayer.GiveOrbSlotOnChannel", method = "Prefix", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.orbs.AbstractOrb" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.characters.AbstractPlayer.ModifyXCostPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.monsters.AbstractMonster", "int" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.characters.AbstractPlayer.OnEvokeOrb$Nested", method = "onEvoke", paramtypes = { "com.megacrit.cardcrawl.orbs.AbstractOrb" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.characters.AbstractPlayer.PostInitializeStarterDeckHookSwitch", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.characters.AbstractPlayer.UseCardModalComplete", method = "Prefix", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.monsters.AbstractMonster", "int" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.core.AbstractCreature.PlayerMaxHPChange$Decrease", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "int", "int[]" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.core.AbstractCreature.PlayerMaxHPChange$Increase", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "int", "boolean", "int[]" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.core.CardCrawlGame.LoadPlayerSaves", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.core.CardCrawlGame", "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.cutscenes.Cutscene.CustomCutscenes", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.cutscenes.Cutscene", "com.megacrit.cardcrawl.characters.AbstractPlayer$PlayerClass" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.cutscenes.Cutscene.CustomVictoryEffects", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.screens.VictoryScreen" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.dungeons.AbstractDungeon.InitializeCardPoolsSwitch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.dungeons.AbstractDungeon", "java.util.ArrayList" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.helpers.RelicLibrary.PopulateLists", method = "Postfix", paramtypes = { "java.util.ArrayList" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.saveAndContinue.SaveFile.ConstructSaveFilePatch", method = "Prefix", paramtypes = { "com.megacrit.cardcrawl.saveAndContinue.SaveFile", "com.megacrit.cardcrawl.saveAndContinue.SaveFile$SaveType" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.charSelect.CharacterOption.CorrectRemainingUnlocks$StringConstructor", method = "correctAmount", paramtypes = { "com.megacrit.cardcrawl.screens.charSelect.CharacterOption", "java.lang.String", "com.megacrit.cardcrawl.characters.AbstractPlayer", "java.lang.String", "java.lang.String" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.charSelect.CharacterOption.CorrectRemainingUnlocks$TextureConstructor", method = "correctAmount", paramtypes = { "com.megacrit.cardcrawl.screens.charSelect.CharacterOption", "java.lang.String", "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.badlogic.gdx.graphics.Texture", "com.badlogic.gdx.graphics.Texture" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.DeathScreen.ForceUnlock", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.screens.GameOverScreen" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.mainMenu.ColorTabBar.ColorTabBarFix$Render", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.screens.mainMenu.ColorTabBar", "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "com.megacrit.cardcrawl.screens.mainMenu.ColorTabBar$CurrentTab" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.NoUnlockBar", method = "unlockLimitCheck", paramtypes = { "com.megacrit.cardcrawl.screens.GameOverScreen", "int", "boolean[]" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.options.OptionsPanel.RefreshSwitch", method = "Postfix", paramtypes = { "java.lang.Object" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.options.SettingsScreen.PopupSwitch", method = "Insert", paramtypes = { "java.lang.Object", "java.lang.Object" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.runHistory.RunHistoryScreen.FixCharacterFilter$ResetRunsDropdown", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.screens.runHistory.RunHistoryScreen", "boolean[]", "com.megacrit.cardcrawl.screens.stats.RunData" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.screens.stats.CharStat.UnlockStats", method = "getLockedCardCount", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "int" }), @SpirePatch2(cls = "basemod.patches.com.megacrit.cardcrawl.unlock.UnlockTracker.CountModdedUnlockCards", method = "countModdedUnlocks", paramtypes = {}), @SpirePatch2(cls = "basemod.screens.ModalChoiceScreen", method = "close", paramtypes = {}), @SpirePatch2(cls = "basemod.screens.ModalChoiceScreen", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.DamageCallbackAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.FetchAction", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.cards.CardGroup", "java.util.function.Predicate", "int", "java.util.function.Consumer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.GainCustomBlockAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.ModifyExhaustiveAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.MoveCardsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.MoveCardsAction", method = "makeText", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.MultiGroupMoveAction", method = "lambda$new$2", paramtypes = { "com.megacrit.cardcrawl.cards.CardGroup$CardGroupType", "java.util.List", "java.util.Map" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.MultiGroupSelectAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.MultiGroupSelectAction", method = "getGroup", paramtypes = { "com.megacrit.cardcrawl.cards.CardGroup$CardGroupType" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.RefundAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.SelectCardsAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.SelectCardsCenteredAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.SelectCardsInHandAction", method = "<ctor>", paramtypes = { "int", "java.lang.String", "boolean", "boolean", "java.util.function.Predicate", "java.util.function.Consumer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.common.SelectCardsInHandAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.defect.EvokeSpecificOrbAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.defect.TriggerPassiveAction", method = "<ctor>", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.defect.TriggerPassiveAction", method = "<ctor>", paramtypes = { "int" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.defect.TriggerPassiveAction", method = "<ctor>", paramtypes = { "int", "int" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.defect.TriggerPassiveAction", method = "<ctor>", paramtypes = { "com.megacrit.cardcrawl.orbs.AbstractOrb", "int" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.defect.TriggerPassiveAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.actions.tempHp.AddTemporaryHPAction", method = "update", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.blockmods.BlockModifierManager", method = "getBlockRetValBasedOnRemainingAmounts", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.cards.targeting.SelfOrEnemyTargeting", method = "updateHovered", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.cards.targeting.SelfOrEnemyTargeting", method = "getDefaultTargetX", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.cards.targeting.SelfOrEnemyTargeting", method = "getDefaultTargetY", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.cards.targeting.SelfOrEnemyTargeting", method = "updateKeyboardTarget", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.AutoplayPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "int", "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.BindingPatches$AddBlockMakePlaceHolderIfNeeded", method = "pls", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "int", "float" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.BindingPatches$AddTempModifiers", method = "addMods", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.BindingPatches$BindObjectToDamageInfo", method = "PostfixMeToPiggybackBinding", paramtypes = { "com.megacrit.cardcrawl.cards.DamageInfo", "com.megacrit.cardcrawl.core.AbstractCreature", "int", "com.megacrit.cardcrawl.cards.DamageInfo$DamageType" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.BlockModifierPatches$OnDirectlyLoseBlock", method = "fixContainers", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "int", "boolean" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.bothInterfaces.BeforeRenderIntentPatches", method = "check", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.bothInterfaces.OnLoseBlockPatch", method = "Prefix", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.megacrit.cardcrawl.cards.DamageInfo", "int[]" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.bothInterfaces.OnPlayerDeathPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.cards.DamageInfo" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.bothInterfaces.OnReceivePowerPatch", method = "CheckPower", paramtypes = { "com.megacrit.cardcrawl.actions.AbstractGameAction", "com.megacrit.cardcrawl.core.AbstractCreature", "com.megacrit.cardcrawl.core.AbstractCreature", "float[]", "com.megacrit.cardcrawl.powers.AbstractPower" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting$RenderReticle", method = "customReticle", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.badlogic.gdx.graphics.g2d.SpriteBatch" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting$SetFinalTarget", method = "setFinalTarget", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting$UseAlternateTargeting", method = "alternateTargeting", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting$UseTargetArrow", method = "cantPlayWithoutTarget", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting$UseTargetArrow", method = "enableTargeting", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting$UseTargetArrow", method = "reallyNeedATarget", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting$UseTargetArrow", method = "keyboardModeAutotarget", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.CustomTargeting", method = "customTargeting", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.evacipated.cardcrawl.mod.stslib.cards.targeting.TargetingHandler" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.DamageModifierPatches$BlockStuff", method = "block", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.DamageModifierPatches$OnAttackMonster", method = "onLastDamageTakenUpdate", paramtypes = { "com.megacrit.cardcrawl.monsters.AbstractMonster", "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.DamageModifierPatches$OnAttackPlayer", method = "onLastDamageTakenUpdate", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.cards.DamageInfo", "int" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.DescriptorAndTooltipPatches$AddTooltipTop", method = "part1", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "java.util.List[]" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.DescriptorAndTooltipPatches$AddTooltipTop", method = "part2", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "java.util.List[]" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.DescriptorAndTooltipPatches$AddTooltipTopSCV", method = "pls", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard", "java.util.ArrayList[]" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.FleetingPatch", method = "Prefix", paramtypes = { "com.megacrit.cardcrawl.actions.utility.UseCardAction", "com.megacrit.cardcrawl.cards.AbstractCard[]", "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.GravePatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.cards.CardGroup", "com.megacrit.cardcrawl.cards.CardGroup", "com.megacrit.cardcrawl.cards.CardGroup" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.MultiGroupGridSelectPatches$HandCardPeekPositions", method = "resetPositions", paramtypes = { "com.megacrit.cardcrawl.screens.select.GridCardSelectScreen" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.powerInterfaces.BetterOnExhaustPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.cards.CardGroup", "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.powerInterfaces.HealthBarRenderPowerPatch$FixRedHealthBar", method = "Prefix", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.powerInterfaces.HealthBarRenderPowerPatch$RenderPowerHealthBar", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float", "float", "float", "float" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.powerInterfaces.OnMyBlockBrokenPowerPatch", method = "Prefix", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.relicInterfaces.BetterOnSmithRelicPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.vfx.campfire.CampfireSmithEffect", "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.relicInterfaces.BetterOnUsePotionPatch", method = "Do", paramtypes = { "com.megacrit.cardcrawl.potions.AbstractPotion" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.relicInterfaces.OnAfterUseCardPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.actions.utility.UseCardAction", "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.relicInterfaces.OnRemoveCardFromMasterDeckPatch", method = "Postfix", paramtypes = { "com.megacrit.cardcrawl.cards.CardGroup", "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.relicInterfaces.OnSkipCardRelicPatch$OnSkipCardPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.ui.buttons.ProceedButton", "com.megacrit.cardcrawl.rewards.RewardItem" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.relicInterfaces.OnSkipCardRelicPatch$SingingBowlSkipPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.ui.buttons.SingingBowlButton", "com.megacrit.cardcrawl.rewards.RewardItem" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.relicInterfaces.RelicOnChannelPatch", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.characters.AbstractPlayer", "com.megacrit.cardcrawl.orbs.AbstractOrb" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.RenderElementsOnCardPatches$RenderOnCardPatch", method = "renderHelper", paramtypes = { "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float", "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.RenderElementsOnCardPatches", method = "validLocation", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.RenderStackedBlockInstances$BlockStackElement", method = "onHover", paramtypes = {}), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.RetainCardsSelectPatch$After", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.actions.unique.RetainCardsAction" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.RetainCardsSelectPatch$Before", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.actions.unique.RetainCardsAction" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.SoulboundPatch$FountainOfCurseRemoval_buttonEffect", method = "canRemove", paramtypes = { "int" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.tempHp.PlayerDamage", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.megacrit.cardcrawl.cards.DamageInfo", "int[]", "boolean[]" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.tempHp.RenderHealthBar", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.tempHp.RenderHealthBar", method = "renderTempHPIconAndValue", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.tempHp.RenderTempHPOutline", method = "Insert", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.patches.tempHp.RenderTempHPOutline", method = "renderTempHPOutline", paramtypes = { "com.megacrit.cardcrawl.core.AbstractCreature", "com.badlogic.gdx.graphics.g2d.SpriteBatch", "float", "float" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.StSLib", method = "getMasterDeckEquivalent", paramtypes = { "com.megacrit.cardcrawl.cards.AbstractCard" }), @SpirePatch2(cls = "com.evacipated.cardcrawl.mod.stslib.StSLib", method = "receiveOnBattleStart", paramtypes = { "com.megacrit.cardcrawl.rooms.AbstractRoom" }) })
    public static class EncapsulateDirectFieldAccess {

        @SpireInstrumentPatch()
        public static ExprEditor updateReferences() {
            return new ExprEditor() {

                @Override
                public void edit(FieldAccess e) throws CannotCompileException {
                    try {
                        CtField accessed = e.getField();
                        if (Modifier.isStatic(accessed.getModifiers()))
                            return;
                        CtClass declarer = accessed.getDeclaringClass();
                        if (!declarer.getName().equals(AbstractPlayer.class.getName()) && !declarer.getName().equals(AbstractCreature.class.getName()))
                            return;
                        if (e.isWriter() && Modifier.isFinal(accessed.getModifiers()))
                            return;
                        if (e.isReader())
                            e.replace("$_ = " + PlayerFields.class.getName().replace('$', '.') + "." + PlayerFields.toPascalCase("get", e.getField()) + "($0);");
                        else if (e.isWriter())
                            e.replace("" + PlayerFields.class.getName().replace('$', '.') + "." + PlayerFields.toPascalCase("set", e.getField()) + "($0, $1);");
                    } catch (NotFoundException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            };
        }
    }

    private static class HiddenCreatureFields {

        private static final Field tips_FIELD, healthHideTimer_FIELD, healthBarWidth_FIELD, targetHealthBarWidth_FIELD, hbShowTimer_FIELD, healthBarAnimTimer_FIELD, blockAnimTimer_FIELD, blockOffset_FIELD, blockScale_FIELD, hbYOffset_FIELD, hbBgColor_FIELD, hbShadowColor_FIELD, blockColor_FIELD, blockOutlineColor_FIELD, blockTextColor_FIELD, redHbBarColor_FIELD, greenHbBarColor_FIELD, blueHbBarColor_FIELD, orangeHbBarColor_FIELD, hbTextColor_FIELD, shakeToggle_FIELD, vX_FIELD, vY_FIELD, animation_FIELD, animationTimer_FIELD, atlas_FIELD, skeleton_FIELD, stateData_FIELD, reticleColor_FIELD, reticleShadowColor_FIELD, reticleOffset_FIELD, reticleAnimTimer_FIELD;

        static {
            try {
                tips_FIELD = AbstractCreature.class.getDeclaredField("tips");
                tips_FIELD.setAccessible(true);
                healthHideTimer_FIELD = AbstractCreature.class.getDeclaredField("healthHideTimer");
                healthHideTimer_FIELD.setAccessible(true);
                healthBarWidth_FIELD = AbstractCreature.class.getDeclaredField("healthBarWidth");
                healthBarWidth_FIELD.setAccessible(true);
                targetHealthBarWidth_FIELD = AbstractCreature.class.getDeclaredField("targetHealthBarWidth");
                targetHealthBarWidth_FIELD.setAccessible(true);
                hbShowTimer_FIELD = AbstractCreature.class.getDeclaredField("hbShowTimer");
                hbShowTimer_FIELD.setAccessible(true);
                healthBarAnimTimer_FIELD = AbstractCreature.class.getDeclaredField("healthBarAnimTimer");
                healthBarAnimTimer_FIELD.setAccessible(true);
                blockAnimTimer_FIELD = AbstractCreature.class.getDeclaredField("blockAnimTimer");
                blockAnimTimer_FIELD.setAccessible(true);
                blockOffset_FIELD = AbstractCreature.class.getDeclaredField("blockOffset");
                blockOffset_FIELD.setAccessible(true);
                blockScale_FIELD = AbstractCreature.class.getDeclaredField("blockScale");
                blockScale_FIELD.setAccessible(true);
                hbYOffset_FIELD = AbstractCreature.class.getDeclaredField("hbYOffset");
                hbYOffset_FIELD.setAccessible(true);
                hbBgColor_FIELD = AbstractCreature.class.getDeclaredField("hbBgColor");
                hbBgColor_FIELD.setAccessible(true);
                hbShadowColor_FIELD = AbstractCreature.class.getDeclaredField("hbShadowColor");
                hbShadowColor_FIELD.setAccessible(true);
                blockColor_FIELD = AbstractCreature.class.getDeclaredField("blockColor");
                blockColor_FIELD.setAccessible(true);
                blockOutlineColor_FIELD = AbstractCreature.class.getDeclaredField("blockOutlineColor");
                blockOutlineColor_FIELD.setAccessible(true);
                blockTextColor_FIELD = AbstractCreature.class.getDeclaredField("blockTextColor");
                blockTextColor_FIELD.setAccessible(true);
                redHbBarColor_FIELD = AbstractCreature.class.getDeclaredField("redHbBarColor");
                redHbBarColor_FIELD.setAccessible(true);
                greenHbBarColor_FIELD = AbstractCreature.class.getDeclaredField("greenHbBarColor");
                greenHbBarColor_FIELD.setAccessible(true);
                blueHbBarColor_FIELD = AbstractCreature.class.getDeclaredField("blueHbBarColor");
                blueHbBarColor_FIELD.setAccessible(true);
                orangeHbBarColor_FIELD = AbstractCreature.class.getDeclaredField("orangeHbBarColor");
                orangeHbBarColor_FIELD.setAccessible(true);
                hbTextColor_FIELD = AbstractCreature.class.getDeclaredField("hbTextColor");
                hbTextColor_FIELD.setAccessible(true);
                shakeToggle_FIELD = AbstractCreature.class.getDeclaredField("shakeToggle");
                shakeToggle_FIELD.setAccessible(true);
                vX_FIELD = AbstractCreature.class.getDeclaredField("vX");
                vX_FIELD.setAccessible(true);
                vY_FIELD = AbstractCreature.class.getDeclaredField("vY");
                vY_FIELD.setAccessible(true);
                animation_FIELD = AbstractCreature.class.getDeclaredField("animation");
                animation_FIELD.setAccessible(true);
                animationTimer_FIELD = AbstractCreature.class.getDeclaredField("animationTimer");
                animationTimer_FIELD.setAccessible(true);
                atlas_FIELD = AbstractCreature.class.getDeclaredField("atlas");
                atlas_FIELD.setAccessible(true);
                skeleton_FIELD = AbstractCreature.class.getDeclaredField("skeleton");
                skeleton_FIELD.setAccessible(true);
                stateData_FIELD = AbstractCreature.class.getDeclaredField("stateData");
                stateData_FIELD.setAccessible(true);
                reticleColor_FIELD = AbstractCreature.class.getDeclaredField("reticleColor");
                reticleColor_FIELD.setAccessible(true);
                reticleShadowColor_FIELD = AbstractCreature.class.getDeclaredField("reticleShadowColor");
                reticleShadowColor_FIELD.setAccessible(true);
                reticleOffset_FIELD = AbstractCreature.class.getDeclaredField("reticleOffset");
                reticleOffset_FIELD.setAccessible(true);
                reticleAnimTimer_FIELD = AbstractCreature.class.getDeclaredField("reticleAnimTimer");
                reticleAnimTimer_FIELD.setAccessible(true);
            } catch (NoSuchFieldException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public interface CreatureHandler<Creature extends AbstractCreature> {

        public abstract Creature getCreature();

        public default String getName() {
            return this.getCreature().name;
        }

        public default void setName(String value) {
            this.getCreature().name = value;
        }

        public default String getId() {
            return this.getCreature().id;
        }

        public default void setId(String value) {
            this.getCreature().id = value;
        }

        public default ArrayList<AbstractPower> getPowers() {
            return this.getCreature().powers;
        }

        public default void setPowers(ArrayList<AbstractPower> value) {
            this.getCreature().powers = value;
        }

        public default boolean getIsPlayer() {
            return this.getCreature().isPlayer;
        }

        public default void setIsPlayer(boolean value) {
            this.getCreature().isPlayer = value;
        }

        public default boolean getIsBloodied() {
            return this.getCreature().isBloodied;
        }

        public default void setIsBloodied(boolean value) {
            this.getCreature().isBloodied = value;
        }

        public default float getDrawX() {
            return this.getCreature().drawX;
        }

        public default void setDrawX(float value) {
            this.getCreature().drawX = value;
        }

        public default float getDrawY() {
            return this.getCreature().drawY;
        }

        public default void setDrawY(float value) {
            this.getCreature().drawY = value;
        }

        public default float getDialogX() {
            return this.getCreature().dialogX;
        }

        public default void setDialogX(float value) {
            this.getCreature().dialogX = value;
        }

        public default float getDialogY() {
            return this.getCreature().dialogY;
        }

        public default void setDialogY(float value) {
            this.getCreature().dialogY = value;
        }

        public default Hitbox getHb() {
            return this.getCreature().hb;
        }

        public default void setHb(Hitbox value) {
            this.getCreature().hb = value;
        }

        public default int getGold() {
            return this.getCreature().gold;
        }

        public default void setGold(int value) {
            this.getCreature().gold = value;
        }

        public default int getDisplayGold() {
            return this.getCreature().displayGold;
        }

        public default void setDisplayGold(int value) {
            this.getCreature().displayGold = value;
        }

        public default boolean getIsDying() {
            return this.getCreature().isDying;
        }

        public default void setIsDying(boolean value) {
            this.getCreature().isDying = value;
        }

        public default boolean getIsDead() {
            return this.getCreature().isDead;
        }

        public default void setIsDead(boolean value) {
            this.getCreature().isDead = value;
        }

        public default boolean getHalfDead() {
            return this.getCreature().halfDead;
        }

        public default void setHalfDead(boolean value) {
            this.getCreature().halfDead = value;
        }

        public default boolean getFlipHorizontal() {
            return this.getCreature().flipHorizontal;
        }

        public default void setFlipHorizontal(boolean value) {
            this.getCreature().flipHorizontal = value;
        }

        public default boolean getFlipVertical() {
            return this.getCreature().flipVertical;
        }

        public default void setFlipVertical(boolean value) {
            this.getCreature().flipVertical = value;
        }

        public default float getEscapeTimer() {
            return this.getCreature().escapeTimer;
        }

        public default void setEscapeTimer(float value) {
            this.getCreature().escapeTimer = value;
        }

        public default boolean getIsEscaping() {
            return this.getCreature().isEscaping;
        }

        public default void setIsEscaping(boolean value) {
            this.getCreature().isEscaping = value;
        }

        public default ArrayList<PowerTip> getTips() {
            try {
                return (ArrayList) HiddenCreatureFields.tips_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setTips(ArrayList<PowerTip> value) {
            try {
                HiddenCreatureFields.tips_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Hitbox getHealthHb() {
            return this.getCreature().healthHb;
        }

        public default void setHealthHb(Hitbox value) {
            this.getCreature().healthHb = value;
        }

        public default float getHealthHideTimer() {
            try {
                return HiddenCreatureFields.healthHideTimer_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHealthHideTimer(float value) {
            try {
                HiddenCreatureFields.healthHideTimer_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default int getLastDamageTaken() {
            return this.getCreature().lastDamageTaken;
        }

        public default void setLastDamageTaken(int value) {
            this.getCreature().lastDamageTaken = value;
        }

        public default float getHbX() {
            return this.getCreature().hb_x;
        }

        public default void setHbX(float value) {
            this.getCreature().hb_x = value;
        }

        public default float getHbY() {
            return this.getCreature().hb_y;
        }

        public default void setHbY(float value) {
            this.getCreature().hb_y = value;
        }

        public default float getHbW() {
            return this.getCreature().hb_w;
        }

        public default void setHbW(float value) {
            this.getCreature().hb_w = value;
        }

        public default float getHbH() {
            return this.getCreature().hb_h;
        }

        public default void setHbH(float value) {
            this.getCreature().hb_h = value;
        }

        public default int getCurrentHealth() {
            return this.getCreature().currentHealth;
        }

        public default void setCurrentHealth(int value) {
            this.getCreature().currentHealth = value;
        }

        public default int getMaxHealth() {
            return this.getCreature().maxHealth;
        }

        public default void setMaxHealth(int value) {
            this.getCreature().maxHealth = value;
        }

        public default int getCurrentBlock() {
            return this.getCreature().currentBlock;
        }

        public default void setCurrentBlock(int value) {
            this.getCreature().currentBlock = value;
        }

        public default float getHealthBarWidth() {
            try {
                return HiddenCreatureFields.healthBarWidth_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHealthBarWidth(float value) {
            try {
                HiddenCreatureFields.healthBarWidth_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getTargetHealthBarWidth() {
            try {
                return HiddenCreatureFields.targetHealthBarWidth_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setTargetHealthBarWidth(float value) {
            try {
                HiddenCreatureFields.targetHealthBarWidth_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getHbShowTimer() {
            try {
                return HiddenCreatureFields.hbShowTimer_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHbShowTimer(float value) {
            try {
                HiddenCreatureFields.hbShowTimer_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getHealthBarAnimTimer() {
            try {
                return HiddenCreatureFields.healthBarAnimTimer_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHealthBarAnimTimer(float value) {
            try {
                HiddenCreatureFields.healthBarAnimTimer_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getBlockAnimTimer() {
            try {
                return HiddenCreatureFields.blockAnimTimer_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setBlockAnimTimer(float value) {
            try {
                HiddenCreatureFields.blockAnimTimer_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getBlockOffset() {
            try {
                return HiddenCreatureFields.blockOffset_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setBlockOffset(float value) {
            try {
                HiddenCreatureFields.blockOffset_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getBlockScale() {
            try {
                return HiddenCreatureFields.blockScale_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setBlockScale(float value) {
            try {
                HiddenCreatureFields.blockScale_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getHbAlpha() {
            return this.getCreature().hbAlpha;
        }

        public default void setHbAlpha(float value) {
            this.getCreature().hbAlpha = value;
        }

        public default float getHbYOffset() {
            try {
                return HiddenCreatureFields.hbYOffset_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHbYOffset(float value) {
            try {
                HiddenCreatureFields.hbYOffset_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getHbBgColor() {
            try {
                return (Color) HiddenCreatureFields.hbBgColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHbBgColor(Color value) {
            try {
                HiddenCreatureFields.hbBgColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getHbShadowColor() {
            try {
                return (Color) HiddenCreatureFields.hbShadowColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHbShadowColor(Color value) {
            try {
                HiddenCreatureFields.hbShadowColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getBlockColor() {
            try {
                return (Color) HiddenCreatureFields.blockColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setBlockColor(Color value) {
            try {
                HiddenCreatureFields.blockColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getBlockOutlineColor() {
            try {
                return (Color) HiddenCreatureFields.blockOutlineColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setBlockOutlineColor(Color value) {
            try {
                HiddenCreatureFields.blockOutlineColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getBlockTextColor() {
            try {
                return (Color) HiddenCreatureFields.blockTextColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setBlockTextColor(Color value) {
            try {
                HiddenCreatureFields.blockTextColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getRedHbBarColor() {
            try {
                return (Color) HiddenCreatureFields.redHbBarColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setRedHbBarColor(Color value) {
            try {
                HiddenCreatureFields.redHbBarColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getGreenHbBarColor() {
            try {
                return (Color) HiddenCreatureFields.greenHbBarColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setGreenHbBarColor(Color value) {
            try {
                HiddenCreatureFields.greenHbBarColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getBlueHbBarColor() {
            try {
                return (Color) HiddenCreatureFields.blueHbBarColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setBlueHbBarColor(Color value) {
            try {
                HiddenCreatureFields.blueHbBarColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getOrangeHbBarColor() {
            try {
                return (Color) HiddenCreatureFields.orangeHbBarColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setOrangeHbBarColor(Color value) {
            try {
                HiddenCreatureFields.orangeHbBarColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getHbTextColor() {
            try {
                return (Color) HiddenCreatureFields.hbTextColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHbTextColor(Color value) {
            try {
                HiddenCreatureFields.hbTextColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default TintEffect getTint() {
            return this.getCreature().tint;
        }

        public default void setTint(TintEffect value) {
            this.getCreature().tint = value;
        }

        public default boolean getShakeToggle() {
            try {
                return HiddenCreatureFields.shakeToggle_FIELD.getBoolean(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setShakeToggle(boolean value) {
            try {
                HiddenCreatureFields.shakeToggle_FIELD.setBoolean(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getAnimX() {
            return this.getCreature().animX;
        }

        public default void setAnimX(float value) {
            this.getCreature().animX = value;
        }

        public default float getAnimY() {
            return this.getCreature().animY;
        }

        public default void setAnimY(float value) {
            this.getCreature().animY = value;
        }

        public default float getVX() {
            try {
                return HiddenCreatureFields.vX_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setVX(float value) {
            try {
                HiddenCreatureFields.vX_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getVY() {
            try {
                return HiddenCreatureFields.vY_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setVY(float value) {
            try {
                HiddenCreatureFields.vY_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default AbstractCreature.CreatureAnimation getAnimation() {
            try {
                return (AbstractCreature.CreatureAnimation) HiddenCreatureFields.animation_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setAnimation(AbstractCreature.CreatureAnimation value) {
            try {
                HiddenCreatureFields.animation_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getAnimationTimer() {
            try {
                return HiddenCreatureFields.animationTimer_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setAnimationTimer(float value) {
            try {
                HiddenCreatureFields.animationTimer_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default TextureAtlas getAtlas() {
            try {
                return (TextureAtlas) HiddenCreatureFields.atlas_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setAtlas(TextureAtlas value) {
            try {
                HiddenCreatureFields.atlas_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Skeleton getSkeleton() {
            try {
                return (Skeleton) HiddenCreatureFields.skeleton_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setSkeleton(Skeleton value) {
            try {
                HiddenCreatureFields.skeleton_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default AnimationState getState() {
            return this.getCreature().state;
        }

        public default void setState(AnimationState value) {
            this.getCreature().state = value;
        }

        public default AnimationStateData getStateData() {
            try {
                return (AnimationStateData) HiddenCreatureFields.stateData_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setStateData(AnimationStateData value) {
            try {
                HiddenCreatureFields.stateData_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getReticleAlpha() {
            return this.getCreature().reticleAlpha;
        }

        public default void setReticleAlpha(float value) {
            this.getCreature().reticleAlpha = value;
        }

        public default Color getReticleColor() {
            try {
                return (Color) HiddenCreatureFields.reticleColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setReticleColor(Color value) {
            try {
                HiddenCreatureFields.reticleColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Color getReticleShadowColor() {
            try {
                return (Color) HiddenCreatureFields.reticleShadowColor_FIELD.get(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setReticleShadowColor(Color value) {
            try {
                HiddenCreatureFields.reticleShadowColor_FIELD.set(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default boolean getReticleRendered() {
            return this.getCreature().reticleRendered;
        }

        public default void setReticleRendered(boolean value) {
            this.getCreature().reticleRendered = value;
        }

        public default float getReticleOffset() {
            try {
                return HiddenCreatureFields.reticleOffset_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setReticleOffset(float value) {
            try {
                HiddenCreatureFields.reticleOffset_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getReticleAnimTimer() {
            try {
                return HiddenCreatureFields.reticleAnimTimer_FIELD.getFloat(this.getCreature());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setReticleAnimTimer(float value) {
            try {
                HiddenCreatureFields.reticleAnimTimer_FIELD.setFloat(this.getCreature(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private static class HiddenPlayerFields {

        private static final Field isHoveringCard_FIELD, hoverStartLine_FIELD, passedHesitationLine_FIELD, isUsingClickDragControl_FIELD, clickDragTimer_FIELD, hoveredMonster_FIELD, skipMouseModeOnce_FIELD, keyboardCardIndex_FIELD, touchscreenInspectCount_FIELD, arrowScale_FIELD, arrowScaleTimer_FIELD, arrowX_FIELD, arrowY_FIELD, points_FIELD, controlPoint_FIELD, arrowTmp_FIELD, startArrowVector_FIELD, endArrowVector_FIELD, renderCorpse_FIELD;

        static {
            try {
                isHoveringCard_FIELD = AbstractPlayer.class.getDeclaredField("isHoveringCard");
                isHoveringCard_FIELD.setAccessible(true);
                hoverStartLine_FIELD = AbstractPlayer.class.getDeclaredField("hoverStartLine");
                hoverStartLine_FIELD.setAccessible(true);
                passedHesitationLine_FIELD = AbstractPlayer.class.getDeclaredField("passedHesitationLine");
                passedHesitationLine_FIELD.setAccessible(true);
                isUsingClickDragControl_FIELD = AbstractPlayer.class.getDeclaredField("isUsingClickDragControl");
                isUsingClickDragControl_FIELD.setAccessible(true);
                clickDragTimer_FIELD = AbstractPlayer.class.getDeclaredField("clickDragTimer");
                clickDragTimer_FIELD.setAccessible(true);
                hoveredMonster_FIELD = AbstractPlayer.class.getDeclaredField("hoveredMonster");
                hoveredMonster_FIELD.setAccessible(true);
                skipMouseModeOnce_FIELD = AbstractPlayer.class.getDeclaredField("skipMouseModeOnce");
                skipMouseModeOnce_FIELD.setAccessible(true);
                keyboardCardIndex_FIELD = AbstractPlayer.class.getDeclaredField("keyboardCardIndex");
                keyboardCardIndex_FIELD.setAccessible(true);
                touchscreenInspectCount_FIELD = AbstractPlayer.class.getDeclaredField("touchscreenInspectCount");
                touchscreenInspectCount_FIELD.setAccessible(true);
                arrowScale_FIELD = AbstractPlayer.class.getDeclaredField("arrowScale");
                arrowScale_FIELD.setAccessible(true);
                arrowScaleTimer_FIELD = AbstractPlayer.class.getDeclaredField("arrowScaleTimer");
                arrowScaleTimer_FIELD.setAccessible(true);
                arrowX_FIELD = AbstractPlayer.class.getDeclaredField("arrowX");
                arrowX_FIELD.setAccessible(true);
                arrowY_FIELD = AbstractPlayer.class.getDeclaredField("arrowY");
                arrowY_FIELD.setAccessible(true);
                points_FIELD = AbstractPlayer.class.getDeclaredField("points");
                points_FIELD.setAccessible(true);
                controlPoint_FIELD = AbstractPlayer.class.getDeclaredField("controlPoint");
                controlPoint_FIELD.setAccessible(true);
                arrowTmp_FIELD = AbstractPlayer.class.getDeclaredField("arrowTmp");
                arrowTmp_FIELD.setAccessible(true);
                startArrowVector_FIELD = AbstractPlayer.class.getDeclaredField("startArrowVector");
                startArrowVector_FIELD.setAccessible(true);
                endArrowVector_FIELD = AbstractPlayer.class.getDeclaredField("endArrowVector");
                endArrowVector_FIELD.setAccessible(true);
                renderCorpse_FIELD = AbstractPlayer.class.getDeclaredField("renderCorpse");
                renderCorpse_FIELD.setAccessible(true);
            } catch (NoSuchFieldException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public interface PlayerHandler<Creature extends AbstractPlayer> extends CreatureHandler<Creature> {

        public abstract Creature getPlayer();

        public default Creature getCreature() {
            return this.getPlayer();
        }

        public default AbstractPlayer.PlayerClass getChosenClass() {
            return this.getPlayer().chosenClass;
        }

        public default void setChosenClass(AbstractPlayer.PlayerClass value) {
            this.getPlayer().chosenClass = value;
        }

        public default int getGameHandSize() {
            return this.getPlayer().gameHandSize;
        }

        public default void setGameHandSize(int value) {
            this.getPlayer().gameHandSize = value;
        }

        public default int getMasterHandSize() {
            return this.getPlayer().masterHandSize;
        }

        public default void setMasterHandSize(int value) {
            this.getPlayer().masterHandSize = value;
        }

        public default int getStartingMaxHP() {
            return this.getPlayer().startingMaxHP;
        }

        public default void setStartingMaxHP(int value) {
            this.getPlayer().startingMaxHP = value;
        }

        public default CardGroup getMasterDeck() {
            return this.getPlayer().masterDeck;
        }

        public default void setMasterDeck(CardGroup value) {
            this.getPlayer().masterDeck = value;
        }

        public default CardGroup getDrawPile() {
            return this.getPlayer().drawPile;
        }

        public default void setDrawPile(CardGroup value) {
            this.getPlayer().drawPile = value;
        }

        public default CardGroup getHand() {
            return this.getPlayer().hand;
        }

        public default void setHand(CardGroup value) {
            this.getPlayer().hand = value;
        }

        public default CardGroup getDiscardPile() {
            return this.getPlayer().discardPile;
        }

        public default void setDiscardPile(CardGroup value) {
            this.getPlayer().discardPile = value;
        }

        public default CardGroup getExhaustPile() {
            return this.getPlayer().exhaustPile;
        }

        public default void setExhaustPile(CardGroup value) {
            this.getPlayer().exhaustPile = value;
        }

        public default CardGroup getLimbo() {
            return this.getPlayer().limbo;
        }

        public default void setLimbo(CardGroup value) {
            this.getPlayer().limbo = value;
        }

        public default ArrayList<AbstractRelic> getRelics() {
            return this.getPlayer().relics;
        }

        public default void setRelics(ArrayList<AbstractRelic> value) {
            this.getPlayer().relics = value;
        }

        public default ArrayList<AbstractBlight> getBlights() {
            return this.getPlayer().blights;
        }

        public default void setBlights(ArrayList<AbstractBlight> value) {
            this.getPlayer().blights = value;
        }

        public default int getPotionSlots() {
            return this.getPlayer().potionSlots;
        }

        public default void setPotionSlots(int value) {
            this.getPlayer().potionSlots = value;
        }

        public default ArrayList<AbstractPotion> getPotions() {
            return this.getPlayer().potions;
        }

        public default void setPotions(ArrayList<AbstractPotion> value) {
            this.getPlayer().potions = value;
        }

        public default EnergyManager getEnergy() {
            return this.getPlayer().energy;
        }

        public default void setEnergy(EnergyManager value) {
            this.getPlayer().energy = value;
        }

        public default boolean getIsEndingTurn() {
            return this.getPlayer().isEndingTurn;
        }

        public default void setIsEndingTurn(boolean value) {
            this.getPlayer().isEndingTurn = value;
        }

        public default boolean getViewingRelics() {
            return this.getPlayer().viewingRelics;
        }

        public default void setViewingRelics(boolean value) {
            this.getPlayer().viewingRelics = value;
        }

        public default boolean getInspectMode() {
            return this.getPlayer().inspectMode;
        }

        public default void setInspectMode(boolean value) {
            this.getPlayer().inspectMode = value;
        }

        public default Hitbox getInspectHb() {
            return this.getPlayer().inspectHb;
        }

        public default void setInspectHb(Hitbox value) {
            this.getPlayer().inspectHb = value;
        }

        public default int getDamagedThisCombat() {
            return this.getPlayer().damagedThisCombat;
        }

        public default void setDamagedThisCombat(int value) {
            this.getPlayer().damagedThisCombat = value;
        }

        public default String getTitle() {
            return this.getPlayer().title;
        }

        public default void setTitle(String value) {
            this.getPlayer().title = value;
        }

        public default ArrayList<AbstractOrb> getOrbs() {
            return this.getPlayer().orbs;
        }

        public default void setOrbs(ArrayList<AbstractOrb> value) {
            this.getPlayer().orbs = value;
        }

        public default int getMasterMaxOrbs() {
            return this.getPlayer().masterMaxOrbs;
        }

        public default void setMasterMaxOrbs(int value) {
            this.getPlayer().masterMaxOrbs = value;
        }

        public default int getMaxOrbs() {
            return this.getPlayer().maxOrbs;
        }

        public default void setMaxOrbs(int value) {
            this.getPlayer().maxOrbs = value;
        }

        public default AbstractStance getStance() {
            return this.getPlayer().stance;
        }

        public default void setStance(AbstractStance value) {
            this.getPlayer().stance = value;
        }

        public default int getCardsPlayedThisTurn() {
            return this.getPlayer().cardsPlayedThisTurn;
        }

        public default void setCardsPlayedThisTurn(int value) {
            this.getPlayer().cardsPlayedThisTurn = value;
        }

        public default boolean getIsHoveringCard() {
            try {
                return HiddenPlayerFields.isHoveringCard_FIELD.getBoolean(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setIsHoveringCard(boolean value) {
            try {
                HiddenPlayerFields.isHoveringCard_FIELD.setBoolean(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default boolean getIsHoveringDropZone() {
            return this.getPlayer().isHoveringDropZone;
        }

        public default void setIsHoveringDropZone(boolean value) {
            this.getPlayer().isHoveringDropZone = value;
        }

        public default float getHoverStartLine() {
            try {
                return HiddenPlayerFields.hoverStartLine_FIELD.getFloat(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHoverStartLine(float value) {
            try {
                HiddenPlayerFields.hoverStartLine_FIELD.setFloat(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default boolean getPassedHesitationLine() {
            try {
                return HiddenPlayerFields.passedHesitationLine_FIELD.getBoolean(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setPassedHesitationLine(boolean value) {
            try {
                HiddenPlayerFields.passedHesitationLine_FIELD.setBoolean(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default AbstractCard getHoveredCard() {
            return this.getPlayer().hoveredCard;
        }

        public default void setHoveredCard(AbstractCard value) {
            this.getPlayer().hoveredCard = value;
        }

        public default AbstractCard getToHover() {
            return this.getPlayer().toHover;
        }

        public default void setToHover(AbstractCard value) {
            this.getPlayer().toHover = value;
        }

        public default AbstractCard getCardInUse() {
            return this.getPlayer().cardInUse;
        }

        public default void setCardInUse(AbstractCard value) {
            this.getPlayer().cardInUse = value;
        }

        public default boolean getIsDraggingCard() {
            return this.getPlayer().isDraggingCard;
        }

        public default void setIsDraggingCard(boolean value) {
            this.getPlayer().isDraggingCard = value;
        }

        public default boolean getIsUsingClickDragControl() {
            try {
                return HiddenPlayerFields.isUsingClickDragControl_FIELD.getBoolean(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setIsUsingClickDragControl(boolean value) {
            try {
                HiddenPlayerFields.isUsingClickDragControl_FIELD.setBoolean(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getClickDragTimer() {
            try {
                return HiddenPlayerFields.clickDragTimer_FIELD.getFloat(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setClickDragTimer(float value) {
            try {
                HiddenPlayerFields.clickDragTimer_FIELD.setFloat(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default boolean getInSingleTargetMode() {
            return this.getPlayer().inSingleTargetMode;
        }

        public default void setInSingleTargetMode(boolean value) {
            this.getPlayer().inSingleTargetMode = value;
        }

        public default AbstractMonster getHoveredMonster() {
            try {
                return (AbstractMonster) HiddenPlayerFields.hoveredMonster_FIELD.get(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setHoveredMonster(AbstractMonster value) {
            try {
                HiddenPlayerFields.hoveredMonster_FIELD.set(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getHoverEnemyWaitTimer() {
            return this.getPlayer().hoverEnemyWaitTimer;
        }

        public default void setHoverEnemyWaitTimer(float value) {
            this.getPlayer().hoverEnemyWaitTimer = value;
        }

        public default boolean getIsInKeyboardMode() {
            return this.getPlayer().isInKeyboardMode;
        }

        public default void setIsInKeyboardMode(boolean value) {
            this.getPlayer().isInKeyboardMode = value;
        }

        public default boolean getSkipMouseModeOnce() {
            try {
                return HiddenPlayerFields.skipMouseModeOnce_FIELD.getBoolean(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setSkipMouseModeOnce(boolean value) {
            try {
                HiddenPlayerFields.skipMouseModeOnce_FIELD.setBoolean(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default int getKeyboardCardIndex() {
            try {
                return HiddenPlayerFields.keyboardCardIndex_FIELD.getInt(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setKeyboardCardIndex(int value) {
            try {
                HiddenPlayerFields.keyboardCardIndex_FIELD.setInt(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default int getTouchscreenInspectCount() {
            try {
                return HiddenPlayerFields.touchscreenInspectCount_FIELD.getInt(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setTouchscreenInspectCount(int value) {
            try {
                HiddenPlayerFields.touchscreenInspectCount_FIELD.setInt(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Texture getImg() {
            return this.getPlayer().img;
        }

        public default void setImg(Texture value) {
            this.getPlayer().img = value;
        }

        public default Texture getShoulderImg() {
            return this.getPlayer().shoulderImg;
        }

        public default void setShoulderImg(Texture value) {
            this.getPlayer().shoulderImg = value;
        }

        public default Texture getShoulder2Img() {
            return this.getPlayer().shoulder2Img;
        }

        public default void setShoulder2Img(Texture value) {
            this.getPlayer().shoulder2Img = value;
        }

        public default Texture getCorpseImg() {
            return this.getPlayer().corpseImg;
        }

        public default void setCorpseImg(Texture value) {
            this.getPlayer().corpseImg = value;
        }

        public default float getArrowScale() {
            try {
                return HiddenPlayerFields.arrowScale_FIELD.getFloat(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setArrowScale(float value) {
            try {
                HiddenPlayerFields.arrowScale_FIELD.setFloat(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getArrowScaleTimer() {
            try {
                return HiddenPlayerFields.arrowScaleTimer_FIELD.getFloat(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setArrowScaleTimer(float value) {
            try {
                HiddenPlayerFields.arrowScaleTimer_FIELD.setFloat(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getArrowX() {
            try {
                return HiddenPlayerFields.arrowX_FIELD.getFloat(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setArrowX(float value) {
            try {
                HiddenPlayerFields.arrowX_FIELD.setFloat(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default float getArrowY() {
            try {
                return HiddenPlayerFields.arrowY_FIELD.getFloat(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setArrowY(float value) {
            try {
                HiddenPlayerFields.arrowY_FIELD.setFloat(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default boolean getEndTurnQueued() {
            return this.getPlayer().endTurnQueued;
        }

        public default void setEndTurnQueued(boolean value) {
            this.getPlayer().endTurnQueued = value;
        }

        public default Vector2[] getPoints() {
            try {
                return (Vector2[]) HiddenPlayerFields.points_FIELD.get(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setPoints(Vector2[] value) {
            try {
                HiddenPlayerFields.points_FIELD.set(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Vector2 getControlPoint() {
            try {
                return (Vector2) HiddenPlayerFields.controlPoint_FIELD.get(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setControlPoint(Vector2 value) {
            try {
                HiddenPlayerFields.controlPoint_FIELD.set(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Vector2 getArrowTmp() {
            try {
                return (Vector2) HiddenPlayerFields.arrowTmp_FIELD.get(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setArrowTmp(Vector2 value) {
            try {
                HiddenPlayerFields.arrowTmp_FIELD.set(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Vector2 getStartArrowVector() {
            try {
                return (Vector2) HiddenPlayerFields.startArrowVector_FIELD.get(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setStartArrowVector(Vector2 value) {
            try {
                HiddenPlayerFields.startArrowVector_FIELD.set(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default Vector2 getEndArrowVector() {
            try {
                return (Vector2) HiddenPlayerFields.endArrowVector_FIELD.get(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setEndArrowVector(Vector2 value) {
            try {
                HiddenPlayerFields.endArrowVector_FIELD.set(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default boolean getRenderCorpse() {
            try {
                return HiddenPlayerFields.renderCorpse_FIELD.getBoolean(this.getPlayer());
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }

        public default void setRenderCorpse(boolean value) {
            try {
                HiddenPlayerFields.renderCorpse_FIELD.setBoolean(this.getPlayer(), value);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static abstract class CustomPlayerPlayerHandler extends CustomPlayer implements PlayerHandler<AbstractPlayer> {

        public AbstractPlayer getPlayer() {
            return this;
        }

        protected CustomPlayerPlayerHandler(String arg0, PlayerClass arg1, EnergyOrbInterface arg2, AbstractAnimation arg3) {
            super(arg0, arg1, arg2, arg3);
        }

        protected CustomPlayerPlayerHandler(String arg0, PlayerClass arg1, String[] arg2, String arg3, float[] arg4, AbstractAnimation arg5) {
            super(arg0, arg1, arg2, arg3, arg4, arg5);
        }

        protected CustomPlayerPlayerHandler(String arg0, PlayerClass arg1, String[] arg2, String arg3, AbstractAnimation arg4) {
            super(arg0, arg1, arg2, arg3, arg4);
        }

        protected CustomPlayerPlayerHandler(String arg0, PlayerClass arg1, String[] arg2, String arg3, String arg4, String arg5) {
            super(arg0, arg1, arg2, arg3, arg4, arg5);
        }

        protected CustomPlayerPlayerHandler(String arg0, PlayerClass arg1, EnergyOrbInterface arg2, String arg3, String arg4) {
            super(arg0, arg1, arg2, arg3, arg4);
        }

        protected CustomPlayerPlayerHandler(String arg0, PlayerClass arg1, String[] arg2, String arg3, float[] arg4, String arg5, String arg6) {
            super(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
        }
    }
}
